package com.example.shinple;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.util.Log;

import androidx.core.app.NotificationCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

//

    public static final String TAG = MyFirebaseMessagingService.class.getSimpleName();

    private String msg, title; //msg와 title


    //메시지가 호출될 때

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Log.e(TAG, "onMessageReceived");
        title = remoteMessage.getNotification().getTitle();
        msg = remoteMessage.getNotification().getBody();

        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(intent.FLAG_ACTIVITY_CLEAR_TOP);

        PendingIntent content_intent = PendingIntent.getActivity(this , 0, new Intent(this, MainActivity.class), 0);
        //PendingIntent - 실질적으로 notification에서 쓰이게 됨. intent를 가지고 있는 클래스지만 다른
        //app의 권한을 허가시켜 본인앱의 프로세스처럼 사용하게 됨
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this).setSmallIcon(R.mipmap.ic_launcher).
                setContentTitle(title).setContentText(msg).setAutoCancel(true).setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                .setVibrate(new long[]{1, 1000});
        //1, 1000 - 1초 동안 진동 울리는 것
        //mimap - push알림 떴을 때 ic_launcher뜨는 것. 아이콘

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);





        notificationManager.notify(0,mBuilder.build());

        mBuilder.setContentIntent(content_intent);








    }



    //    @Override
//    public void onNewToken(String s) {
//        String token = FirebaseInstanceId.getInstance().getToken();
//        Log.e(TAG, token);
//        sendRegistrationToServer(token);
//
//
//    }
    public void onNewToken(String token) {
        Log.d(TAG, "Refreshed token: " + token);

        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // Instance ID token to your app server.
        sendRegistrationToServer(token);
    }


    private void sendRegistrationToServer(String token) {
        // TODO: Implement this method to send token to your app server.
        //앱서버에 토큰을 보내는 것이다.

    }
    //
}



//
//
//
//    private String msg, title; //msg와 title
//
//
//    //메시지가 호출될 때
//
//    @Override
//    public void onMessageReceived(RemoteMessage remoteMessage) {
//        Log.e(TAG, "onMessageReceived");
//        title = remoteMessage.getNotification().getTitle();
//        msg = remoteMessage.getNotification().getBody();
//
//        Intent intent = new Intent(this, MainActivity.class);
//        intent.addFlags(intent.FLAG_ACTIVITY_CLEAR_TOP);
//
//        PendingIntent content_intent = PendingIntent.getActivity(this , 0, new Intent(this, MainActivity.class), 0);
//        //PendingIntent - 실질적으로 notification에서 쓰이게 됨. intent를 가지고 있는 클래스지만 다른
//        //app의 권한을 허가시켜 본인앱의 프로세스처럼 사용하게 됨
//        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this).setSmallIcon(R.mipmap.ic_launcher).
//                setContentTitle(title).setContentText(msg).setAutoCancel(true).setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
//                .setVibrate(new long[]{1, 1000});
//        //1, 1000 - 1초 동안 진동 울리는 것
//        //mimap - push알림 떴을 때 ic_launcher뜨는 것. 아이콘
//
//        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
//
//
//
//
//
//        notificationManager.notify(0,mBuilder.build());
//
//        mBuilder.setContentIntent(content_intent);
//
//
//
//
//
//
//
//
//    }
//
//    public MyFirebaseMessagingService() {
//        super();
//    }
//
//    @Override
//    public void onDeletedMessages() {
//        super.onDeletedMessages();
//    }
//
//    @Override
//    public void onMessageSent(String s) {
//        super.onMessageSent(s);
//    }
//
//    @Override
//    public void onSendError(String s, Exception e) {
//        super.onSendError(s, e);
//    }
//
//    //    @Override
////    public void onNewToken(String s) {
////        String token = FirebaseInstanceId.getInstance().getToken();
////        Log.e(TAG, token);
////        sendRegistrationToServer(token);
////
////
////    }
//    public void onNewToken(String token) {
//        Log.d(TAG, "Refreshed token: " + token);
//
//        // If you want to send messages to this application instance or
//        // manage this apps subscriptions on the server side, send the
//        // Instance ID token to your app server.
//        sendRegistrationToServer(token);
//    }
//
//
//    private void sendRegistrationToServer(String token) {
//        // TODO: Implement this method to send token to your app server.
//        //앱서버에 토큰을 보내는 것이다.
//
//    }
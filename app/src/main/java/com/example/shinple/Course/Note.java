package com.example.shinple.Course;

public class Note {

    public String title;
    public String date;

    public Note(String title, String date ){
        this.title = title;
        this.date = date;


    }



    public String getTitle() {
        return title;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}

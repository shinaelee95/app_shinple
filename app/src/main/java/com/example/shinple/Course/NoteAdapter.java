package com.example.shinple.Course;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.shinple.R;

import java.util.List;

    public class NoteAdapter extends BaseAdapter {

        private Context context;
        private List<Note> notelist;

        public NoteAdapter(Context context, List<Note> notelist){
            this.context = context;
            this.notelist = notelist;
        }

    @Override
    public int getCount() {
        return notelist.size();
    }

    @Override
    public Object getItem(int i) {
        return notelist.get(i);
    }

    @Override
    public long getItemId(int i) {
       return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View v = View.inflate(context, R.layout.note_status, null);
        TextView note_title = (TextView) v.findViewById(R.id.note_title);
        TextView note_date = (TextView) v.findViewById(R.id.note_date);
        note_title.setText(notelist.get(i).getTitle());
        note_date.setText(notelist.get(i).getDate());


        v.setTag(notelist.get(i).getTitle());
        return v;
    }



}


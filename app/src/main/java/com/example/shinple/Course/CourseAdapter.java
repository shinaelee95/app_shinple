package com.example.shinple.Course;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.shinple.R;

import java.util.List;

public class CourseAdapter extends BaseAdapter {

    private Context context;
    private List<Course> courselist;

    public CourseAdapter(Context context, List<Course> courselist){
        this.context = context;
        this.courselist = courselist;
    }


    @Override
    public int getCount() {
        return courselist.size();
    }

    @Override
    public Object getItem(int i) {
        return courselist.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View v = View.inflate(context, R.layout.course_status, null);
        TextView note_title = (TextView) v.findViewById(R.id.course_title);

        note_title.setText(courselist.get(i).getTitle());



        v.setTag(courselist.get(i).getTitle());
        return v;
    }

}

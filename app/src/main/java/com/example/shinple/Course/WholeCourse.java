package com.example.shinple.Course;

import android.widget.ImageView;

public class WholeCourse {

//    private Drawable iconDrawable ;
    private String categoryStr;
    private String titleStr ;
    private String instructorStr;
    private String course_id;

    public String getCourse_id() {
        return course_id;
    }

    public void setCourse_id(String course_id) {
        this.course_id = course_id;
    }

    public ImageView getPicassoImageView() {
        return picassoImageView;
    }

    public void setPicassoImageView(ImageView picassoImageView) {
        this.picassoImageView = picassoImageView;
    }

    private String thumbnail;
    private ImageView picassoImageView;

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }




//    public void setIcon(Drawable icon) {
//        iconDrawable = icon ;
//    }
    public void setCategory(String categoryStr) {
        this.categoryStr = categoryStr ;
    }
    public void setTitle(String title) {
        titleStr = title ;
    }
    public void setInstructor(String instructor) {
        instructorStr = instructor ;
    }

//    public Drawable getIcon() {
//        return this.iconDrawable ;
//    }
    public String getCategory() {
        return this.categoryStr ;
    }
    public String getTitle() {
        return this.titleStr ;
    }
    public String getInstructor() {
        return this.instructorStr ;
    }

}

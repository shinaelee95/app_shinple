package com.example.shinple.Course;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.VideoView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.Volley;
import com.example.shinple.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class VideoFragmentOne extends Fragment {
    private ListView course_list_view;
    private CourseAdapter courseAdapter;
    private List<Course> courseList;
    public String c_id;


    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.video_fragment_one, container, false);
        //----------------필요한 내용 추가-------------------//
        course_list_view = (ListView) view.findViewById(R.id.course_table);

       c_id = ((VideoActivity)getActivity()).getC_id();






        Response.Listener<String> responseListener = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonResponse = new JSONObject(response);
                    JSONArray contents = jsonResponse.getJSONArray("result");
                    courseList = new ArrayList<>();
                    for (int i = 0; i < contents.length(); i++) {
                        JSONObject obj = contents.getJSONObject(i);
                        String title = obj.getString("v_title");
                        String videoPath = obj.getString("video_path");
                        String v_id = obj.getString("v_id");
                        courseList.add(new Course(title,videoPath,v_id));

                    }
                    courseAdapter = new CourseAdapter(getActivity(), courseList);
                    course_list_view.setAdapter(courseAdapter);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        };

        CourseListRequest courseListRequest = new CourseListRequest(c_id, responseListener);
        RequestQueue queue = Volley.newRequestQueue(container.getContext());
        queue.add(courseListRequest);

        course_list_view.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView parent, View v, int position, long id) {
                String course_title = courseList.get(position).getTitle();
                TextView course_title_view = (TextView)getActivity().findViewById(R.id.tv_course_id);
                course_title_view.setText(course_title);
                String video_path = courseList.get(position).getVideoPath();
                String v_id = courseList.get(position).getV_id();
                ((VideoActivity)getActivity()).settingChanged(v_id,video_path);
                VideoView videoView = (VideoView)getActivity().findViewById(R.id.course_video_view);
                videoView.pause();
                videoView.setVideoPath(video_path);
                final MediaController mediaController = new MediaController(getActivity());
                videoView.setMediaController(mediaController);
                mediaController.show();
                videoView.start();



            }
        });









       // return view;
        return view;
    }



}

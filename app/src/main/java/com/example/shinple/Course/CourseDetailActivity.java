package com.example.shinple.Course;


import androidx.appcompat.app.AppCompatActivity;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.DragEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.shinple.Adapter.CourseDetailListViewAdapter;
import com.example.shinple.Adapter.DetailCourseAdapter;
import com.example.shinple.App_DB.DetailCourseConnection;
import com.example.shinple.App_DB.InsertCourseConnection;
import com.example.shinple.Functions.DetailCourseFunc;
import com.example.shinple.ListVO.DetailCourseVO;
import com.example.shinple.MainActivity;
import com.example.shinple.MainMenu2Activity;
import com.example.shinple.MainMenuActivity;
import com.example.shinple.MyClass.VideoListActivity;
import com.example.shinple.Note.NoteActivity;
import com.example.shinple.Notice.NoticeActivity;
import com.example.shinple.R;
import com.nightonke.boommenu.BoomButtons.OnBMClickListener;
import com.nightonke.boommenu.BoomButtons.TextOutsideCircleButton;
import com.nightonke.boommenu.BoomMenuButton;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Map;

public class CourseDetailActivity extends AppCompatActivity implements View.OnDragListener {
    private final int START_DRAG = 0;
    private final int END_DRAG = 1;
    private int isMoving;
    private float offset_x, offset_y;
    private boolean start_yn = true;
    private ListView courseDetailListView ;
    public Map<String, String> courseData;
    public ArrayList<Map<String, String>> videoList = new ArrayList<>();
    DetailCourseVO detailCourseData = new DetailCourseVO();
    //WholeCourse wc = new WholeCourse();
    DetailCourseAdapter adapter;
    CourseDetailListViewAdapter cAdapter;
    BoomMenuButton bmb;
    String subCategory, title;
    int courseId, u_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_course_detail);

        Intent intent = getIntent(); /*데이터 수신*/
        //변수 초기화
        adapter = new DetailCourseAdapter();
        cAdapter = new CourseDetailListViewAdapter();
        courseDetailListView = (ListView) findViewById(R.id.detail_video_listview);
        courseDetailListView.setAdapter(cAdapter);

        subCategory = intent.getExtras().getString("subCategory"); /*String형*/
        title = intent.getExtras().getString("title"); /*String형*/
        courseId = intent.getExtras().getInt("courseId"); /*String형*/
        u_id = intent.getExtras().getInt("u_id"); /*String형*/
        String videoListTitle = "["+subCategory +"] " + title;

        // URL 연결 - 서버에서 json데이터 받아오기
        DetailCourseConnection url = new DetailCourseConnection();

        //url.setTotal(u_id, c_id);
        url.setTotal(u_id, courseId);

        url.start();
        try {
            url.join();
        }
        catch(Exception e){
            e.printStackTrace();
        }


        String result = url.getTemp();
        System.out.println("URL result : "  + result);

        detailCourseData = DetailCourseFunc.parseDetailCourseJSON(result);
        videoList = DetailCourseFunc.parseDetailCourseVideoJSON(result);

        cAdapter =  DetailCourseFunc.setVideoListData(cAdapter, videoList);


        TextView tv_course_contents_title =(TextView)findViewById(R.id.tv_course_contents_title);
        TextView tv_course_contents = (TextView)findViewById(R.id.tv_course_contents);
        ImageView picassoImageView = (ImageView) findViewById(R.id.course_thumb_detail_image);

        // String title =  "["+detailCourseData.getSubCategory()+"] "+detailCourseData.getTitle();
        tv_course_contents_title.setText(videoListTitle);
        tv_course_contents.setText(detailCourseData.getCourseInfo());


        Picasso.with(this)
                .load(detailCourseData.getThumbnail())
                .into(picassoImageView);


        Button buttonBlue = (Button) findViewById(R.id.button) ;
        buttonBlue.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View view) {

                InsertCourseConnection insertUrl= new InsertCourseConnection();
                insertUrl.connInsertCourse(u_id,  detailCourseData.getCourseId(), 0);

                insertUrl.start();
                try {
                    insertUrl.join();
                }
                catch(Exception e){
                    e.printStackTrace();
                }

                String result = insertUrl.getTemp();

                try{
                    JSONObject obj = new JSONObject(result);

                    String jsonRes = obj.getString("status");

                    if(jsonRes.equals("OK")){
                        System.out.println(" 수강하기 ");
                        Intent intent = new Intent(getApplicationContext(), VideoListActivity.class);

                        System.out.println("List - sub : " +  detailCourseData.getSubCategory() +", title : " + detailCourseData.getTitle());
                        System.out.println("Intent sub : " +  subCategory +", title : " + title + "courseId " + detailCourseData.getCourseId() + " u_id" + u_id);

                        intent.putExtra("subCategory",subCategory );
                        intent.putExtra("title",title );
                        intent.putExtra("courseId",courseId );
                        intent.putExtra("u_id",u_id );

                        startActivity(intent);
                    }
                }catch (JSONException e)
                {
                    System.out.println("json exception");
                    e.printStackTrace();
                }

            }
        }) ;

        settingBoommenu();
        bmb.setDraggable(true);   // 붐 메뉴 버튼 드래그 가능하도록 설정
        bmb.setOnDragListener(this);

    }// OnCreate End

    public void settingBoommenu() {
        bmb = (BoomMenuButton) findViewById(R.id.bmb1);
        bmb.setNormalColor(getResources().getColor(R.color.colorPrimaryP));
        //마이 페이지 버튼 구현
        TextOutsideCircleButton.Builder home_builder = new TextOutsideCircleButton.Builder().normalImageRes(R.drawable.mypage1);
        home_builder.normalText("메인메뉴");
        home_builder.normalColorRes(R.color.white);
        home_builder.normalTextColorRes(R.color.colorNull);
        home_builder.listener(new OnBMClickListener() {
            @Override
            public void onBoomButtonClick(int index) {
                Intent home_intent = new Intent(CourseDetailActivity.this,MainMenu2Activity.class);
                startActivity(home_intent);

            }
        });
        bmb.addBuilder(home_builder);

        //전체 강좌 버튼 구현
        TextOutsideCircleButton.Builder lecture_builder = new TextOutsideCircleButton.Builder().normalImageRes(R.drawable.lists);
        lecture_builder.normalText("전체강좌");
        lecture_builder.normalColorRes(R.color.white);
        lecture_builder.normalTextColorRes(R.color.colorNull);
        lecture_builder.listener(new OnBMClickListener() {
            @Override
            public void onBoomButtonClick(int index) {
                Intent course_intent = new Intent(CourseDetailActivity.this, CourseActivity.class);
                startActivity(course_intent);

            }
        });
        bmb.addBuilder(lecture_builder);
        //공지 사항 버튼 구현
        TextOutsideCircleButton.Builder notice_builder = new TextOutsideCircleButton.Builder().normalImageRes(R.drawable.notice);
        notice_builder.normalText("공지사항");
        notice_builder.normalColorRes(R.color.white);
        notice_builder.normalTextColorRes(R.color.colorNull);
        notice_builder.listener(new OnBMClickListener() {
            @Override
            public void onBoomButtonClick(int index) {
                Intent notice_intent = new Intent(CourseDetailActivity.this, NoticeActivity.class);
                startActivity(notice_intent);
            }
        });
        bmb.addBuilder(notice_builder);
        //강의노트 게시판 버튼 구현
        TextOutsideCircleButton.Builder note_builder = new TextOutsideCircleButton.Builder().normalImageRes(R.drawable.note);
        note_builder.normalText("강의노트");
        note_builder.normalColorRes(R.color.white);
        note_builder.normalTextColorRes(R.color.colorNull);
        note_builder.listener(new OnBMClickListener() {
            @Override
            public void onBoomButtonClick(int index) {
                Intent note_intent = new Intent(CourseDetailActivity.this, NoteActivity.class);
                startActivity(note_intent);
            }
        });
        bmb.addBuilder(note_builder);


    }

    public boolean onDrag(View v, DragEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            if (start_yn) {
                offset_x = event.getX();
                offset_y = event.getY();
                start_yn = false;
            }
            isMoving = START_DRAG;
        } else if (event.getAction() == MotionEvent.ACTION_UP) {
            isMoving = END_DRAG;
        } else if (event.getAction() == MotionEvent.ACTION_MOVE) {
            if (isMoving == START_DRAG) {
                v.setX((int) event.getX() - offset_x);
                v.setY((int) event.getY() - offset_y);
            }
        }
        return false;
    }






}

package com.example.shinple.Course;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.Volley;
import com.example.shinple.R;
import com.google.android.material.textfield.TextInputEditText;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class VideoFragmentTwo extends Fragment {

    private ListView note_list_View;
    private NoteAdapter noteAdapter;
    private List<Note> noteList;

    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.video_fragment_two, container, false);
        note_list_View = (ListView) view.findViewById(R.id.note_list);

        String u_id = ((VideoActivity)getActivity()).getU_id();
        String c_id = ((VideoActivity)getActivity()).getC_id();
        String v_id = ((VideoActivity)getActivity()).getV_id();

        Response.Listener<String> responseListener = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonResponse = new JSONObject(response);
                    JSONArray contents = jsonResponse.getJSONArray("result");
                    noteList = new ArrayList<>();
                    for (int i = 0; i < contents.length(); i++) {
                        JSONObject obj = contents.getJSONObject(i);
                        String classifier = obj.getString("chk_user");
                        String top10_content = obj.getString("note");
                        if(classifier.equals("1"))
                            noteList.add(new Note("나의 노트", top10_content));



                    }
                    noteAdapter = new NoteAdapter(getActivity(), noteList);
                    note_list_View.setAdapter(noteAdapter);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        };

        NoteListRequest noteListRequest = new NoteListRequest(u_id, c_id, v_id, responseListener);
        RequestQueue queue = Volley.newRequestQueue(container.getContext());
        queue.add(noteListRequest);

        note_list_View.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView parent, View v, int position, long id) {
                String note = noteList.get(position).getDate();
                TextInputEditText note_edit = (TextInputEditText)getActivity().findViewById(R.id.note);
                note_edit.setText(null);
                note_edit.setText(note);
            }
        });


        return view;
    }
}


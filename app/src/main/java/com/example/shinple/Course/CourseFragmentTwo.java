package com.example.shinple.Course;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.shinple.R;

public class CourseFragmentTwo extends Fragment {
    FrameLayout jobt1, jobt2, jobt3, jobt4;
    View viewjobt1, viewjobt2, viewjobt3, viewjobt4;
    TextView tvjobt1, tvjobt2, tvjobt3, tvjobt4;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View fragment_two = inflater.inflate(R.layout.course_fragment_two, container, false);
        //INIT VIEWS
        init(fragment_two);
        //SET TABS ONCLICK
        jobt1.setOnClickListener(clik);
        jobt2.setOnClickListener(clik);
        jobt3.setOnClickListener(clik);
//        jobt4.setOnClickListener(clik);
        //LOAD PAGE FOR FIRST
        loadPage(new CapabilityOne());
        tvjobt1.setTextColor(getActivity().getResources().getColor(R.color.colorPrimary));
        return fragment_two;
    }

    public void init(View v) {
        jobt1 = v.findViewById(R.id.jobt1);
        jobt2 = v.findViewById(R.id.jobt2);
        jobt3 = v.findViewById(R.id.jobt3);
//        jobt4 = v.findViewById(R.id.jobt4);
        viewjobt1 = v.findViewById(R.id.viewjobt1);
        viewjobt2 = v.findViewById(R.id.viewjobt2);
        viewjobt3 = v.findViewById(R.id.viewjobt3);
//        viewjobt4 = v.findViewById(R.id.viewjobt4);
        tvjobt1 = v.findViewById(R.id.tvjobt1);
        tvjobt2 = v.findViewById(R.id.tvjobt2);
        tvjobt3 = v.findViewById(R.id.tvjobt3);
//        tvjobt4 = v.findViewById(R.id.tvjobt4);
    }


    //ONCLICK LISTENER
    public View.OnClickListener clik = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.jobt1:
                    //ONSELLER CLICK
                    //LOAD Whole FRAGMENT CLASS
                    loadPage(new CapabilityOne());


                    //WHEN CLICK TEXT COLOR CHANGED
                    tvjobt1.setTextColor(getActivity().getResources().getColor(R.color.Primary));
                    tvjobt2.setTextColor(getActivity().getResources().getColor(R.color.colorPrimaryDark));
                    tvjobt3.setTextColor(getActivity().getResources().getColor(R.color.colorPrimaryDark));
//                    tvjobt4.setTextColor(getActivity().getResources().getColor(R.color.colorPrimaryDark));
                    //VIEW VISIBILITY WHEN CLICKED
                    viewjobt1.setVisibility(View.VISIBLE);
                    viewjobt2.setVisibility(View.INVISIBLE);
                    viewjobt3.setVisibility(View.INVISIBLE);
//                    viewjobt4.setVisibility(View.INVISIBLE);
                    break;
                case R.id.jobt2:
                    //ONBUYER CLICK
                    //LOAD Learn FRAGMENT CLASS
                    loadPage(new CapabilityTwo());


                    //WHEN CLICK TEXT COLOR CHANGED
                    tvjobt1.setTextColor(getActivity().getResources().getColor(R.color.colorPrimaryDark));
                    tvjobt2.setTextColor(getActivity().getResources().getColor(R.color.Primary));
                    tvjobt3.setTextColor(getActivity().getResources().getColor(R.color.colorPrimaryDark));
//                    tvjobt4.setTextColor(getActivity().getResources().getColor(R.color.colorPrimaryDark));
                    //VIEW VISIBILITY WHEN CLICKED
                    viewjobt1.setVisibility(View.INVISIBLE);
                    viewjobt2.setVisibility(View.VISIBLE);
                    viewjobt3.setVisibility(View.INVISIBLE);
//                    viewjobt4.setVisibility(View.INVISIBLE);
                    break;

                case R.id.jobt3:
                    //ONBUYER CLICK
                    //LOAD Complete FRAGMENT CLASS
                    loadPage(new CapabilityThree());


                    //WHEN CLICK TEXT COLOR CHANGED
                    tvjobt1.setTextColor(getActivity().getResources().getColor(R.color.colorPrimaryDark));
                    tvjobt2.setTextColor(getActivity().getResources().getColor(R.color.colorPrimaryDark));
                    tvjobt3.setTextColor(getActivity().getResources().getColor(R.color.Primary));
//                    tvjobt4.setTextColor(getActivity().getResources().getColor(R.color.colorPrimaryDark));
                    //VIEW VISIBILITY WHEN CLICKED
                    viewjobt1.setVisibility(View.INVISIBLE);
                    viewjobt2.setVisibility(View.INVISIBLE);
                    viewjobt3.setVisibility(View.VISIBLE);
//                    viewjobt4.setVisibility(View.INVISIBLE);
                    break;

//                case R.id.jobt4:
//                    //ONBUYER CLICK
//                    //LOAD Expired FRAGMENT CLASS
//                    loadPage(new CapabilityFour());
//
//                    //WHEN CLICK TEXT COLOR CHANGED
//                    tvjobt1.setTextColor(getActivity().getResources().getColor(R.color.colorPrimaryDark));
//                    tvjobt2.setTextColor(getActivity().getResources().getColor(R.color.colorPrimaryDark));
//                    tvjobt3.setTextColor(getActivity().getResources().getColor(R.color.colorPrimaryDark));
//                    tvjobt4.setTextColor(getActivity().getResources().getColor(R.color.Primary));
//                    //VIEW VISIBILITY WHEN CLICKED
//                    viewjobt1.setVisibility(View.INVISIBLE);
//                    viewjobt2.setVisibility(View.INVISIBLE);
//                    viewjobt3.setVisibility(View.INVISIBLE);
//                    viewjobt4.setVisibility(View.VISIBLE);
//                    break;
            }

        }

    };

    //LOAD PAGE FRAGMENT METHOD
    private boolean loadPage(Fragment fragment) {
        if (fragment != null) {
            getActivity().getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.containerpage, fragment)
                    .commit();
            return true;
        }
        return false;
    }
}

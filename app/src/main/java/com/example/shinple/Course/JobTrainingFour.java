//package com.example.shinple.Course;
//
//import android.os.Bundle;
//import android.os.Handler;
//import android.os.Looper;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.ListView;
//import android.widget.TextView;
//
//import androidx.annotation.NonNull;
//import androidx.annotation.Nullable;
//import androidx.fragment.app.Fragment;
//
//import com.example.shinple.Adapter.FullCourseAdapter;
//import com.example.shinple.App_DB.RestClient;
//import com.example.shinple.R;
//
//import org.json.JSONArray;
//import org.json.JSONObject;
//
//import java.util.ArrayList;
//import java.util.List;
//
//public class JobTrainingFour extends Fragment {
//    //    private ListView qna_listView;
////    private QnaListAdapter madapter;
////    private List<Qna> qnaList;
////    ImageView enterqna;
//    private final static int REQUEST_QNA_WRITE = 100;
//
//    String u_id;
//
//    FullCourseAdapter adpapter;
//    ListView job1_listview;
//    int main_id = 1;
//    int sub_id  = 4;
//
//
//    TextView[] textResponse = new TextView[3];  //원하는 TextView 크기 지정
//    String[] text_column = new String[3];
//    RestClient restClient; //RestClient class를 이용해야함 필요 시 만들어서 사용
//
////    TextView course_title; //내용
////    TextView course_category; //카테고리
////    TextView course_instructor;
//
//    View view;
//
//    private Handler mHandler = new Handler(Looper.getMainLooper());
//
//
//    @Nullable
//    @Override
//    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        view = inflater.inflate(R.layout.job_training_one, container, false);
//
//
////
////        setHasOptionsMenu(true);
//
//
//
//
//        init(view);
//
//        return view;
//    }
//
//
//
//
//    // SSL 통신 시 필요 : DB 추가 부분 -----------
//    // //-------------------------------------------------------------------------------------------------------------
//
//    private void init(View view) {
//        getViews(view);
//        restClient = RestClient.getInstance();
//        restClient.parseDB("https://shinple.kr/app_db/course_tbl_by_main_and_sub.php/?main_id="+main_id+"&sub_id="+sub_id);
//        // 불러오고 싶은 DB 테이블 설정!!
//        setListeners();
//    }
//
//    private void getViews(View view) {
//
//
//        textResponse[0] = (TextView) view.findViewById(R.id.coursecategory);
//        textResponse[1] = (TextView) view.findViewById(R.id.coursetitle);
//        textResponse[2] = (TextView) view.findViewById(R.id.courseinstructor);
//        text_column[0] = "c_name";
//        text_column[1] = "c_info";
//        text_column[2] = "instructor";
//    }
//
//    private void setListeners() {                                   // request에 따른 response 함수
//        new Thread(new Runnable() {
//            @Override
//            public void run() {
//                String response = restClient.postRequest(); // response 내용 저장
//                setText(response, textResponse); //파씽 함수 고고
//                System.out.println(response);
//            }
//        }).start();
//    }
//
//    //.runOnUiThread
//    private void setText(final String response, final TextView[] textRes) {
//        mHandler.post(new Runnable() {
//            @Override
//            public void run() {
////                MyQna_SslFunc.ParseJSON(response, textRes, text_column);
//
//                job1_listview = (ListView) view.findViewById(R.id.job_training_one);
//                ArrayList<WholeCourse> jobList = new ArrayList<WholeCourse>();
//                adpapter = new FullCourseAdapter(getActivity(), jobList);
//
//                //listView의 item클릭시 해당 Q&A 뷰잉
////                job1_listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
////                    @Override
////                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
////                        //Qna qna = (Qna) adpapter.getItem(i);
////                        Intent intent2 = new Intent(getActivity(), Qna_view.class);
////                        intent2.putExtra("detail", i);
////                        intent2.putExtra("u_id", u_id);
////                        startActivity(intent2);
////                    }
////                });
//
//                try {
//
//                    JSONObject json = new JSONObject(response);
//                    JSONArray arr = json.getJSONArray("result");
//                    //임의의 리스트를 지정하여 값을 넣어주는 틀을 만듭니다.
//                    final List<String> category = new ArrayList<String>();
//                    final List<String> title = new ArrayList<String>();
//                    final List<String> instructor = new ArrayList<String>();
//
//                    JSONObject json2;
//
//                    for (int i = 0; i < arr.length(); i++) {
//                        json2 = arr.getJSONObject(i);
//                        String course_category = json2.getString(text_column[0]);
//                        String course_title = json2.getString(text_column[1]);
//                        String course_instructor = json2.getString(text_column[2]);
//                        //Qna_SslFunc에서 가져온 것들. json 이용.
//
////                        String cnum = qna_category;
//
////                        switch(cnum){
////                            case "0" :
////                                qna_category = "[강좌]";
////                                break;
////                            case "1" :
////                                qna_category = "[영상]";
////                                break;
////                            case "2" :
////                                qna_category = "[노트]";
////                                break;
////                            case "3" :
////                                qna_category = "[기타]";
////                                break;
////                            default:
////                                break;
////                        }
//
//                        category.add(course_category);
//                        title.add(course_title);
//                        instructor.add(course_instructor);
//                    }
//
//                    for (int i = 0; i < arr.length(); i++) {
//                        jobList.add(new WholeCourse(category.get(i), title.get(i), instructor.get(i)));
//
//
//                    }
//                    job1_listview.setAdapter(adpapter); //adapter에 추가해줍니다.
//
//
//
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }
//        });
//    }
//
//    @Override
//    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
//        super.onViewCreated(view, savedInstanceState);
//    }
//
////    public void onActivityResult(int requestCode, int resultCode, Intent data) {
////        super.onActivityResult(requestCode, resultCode, data);
////        //refresh
////        if (requestCode == REQUEST_QNA_WRITE) {
////            synchronized (adpapter){
////                adpapter.notifyAll();
////            }
////            init(view);
////        }
////    }
//}
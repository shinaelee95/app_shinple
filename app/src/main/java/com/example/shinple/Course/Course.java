package com.example.shinple.Course;

public class Course {

    public String title;
    public String videoPath;
    String v_id;

    public Course(String title , String videoPath, String v_id){
        this.title = title;
        this.videoPath = videoPath;
        this.v_id = v_id;


    }
    public String getVideoPath(){
        return videoPath;
    }

    public void setVideoPath(String videoPath){
        this.videoPath = videoPath;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getV_id(){
        return v_id;
    }

    public void setV_id(String v_id){
        this.v_id = v_id;
    }
}

package com.example.shinple.Course;

import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.SeekBar;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.viewpager.widget.ViewPager;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.Volley;
import com.example.shinple.MainMenu2Activity;
import com.example.shinple.R;
import com.google.android.material.tabs.TabLayout;
import com.google.android.material.textfield.TextInputEditText;

import org.json.JSONException;
import org.json.JSONObject;

//-------1. 추후 비디오 경로 변수로 넣어야함 서버에서 받아서--------
//-------2.
public class VideoActivity extends AppCompatActivity {
    VideoView videoView;
    TextInputEditText note;
    Switch note_sw;
    SeekBar note_sb;
    ViewPager viewPager;
    TabLayout tabLayout;
    String v_id;
    String c_id;
    String videoPath;
    String u_id;
    String titleStr;
    TextView video_title;




    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video); // 레이아웃
        Intent intent = getIntent();
        v_id = intent.getStringExtra("v_id");
        c_id = intent.getStringExtra("c_id");
        videoPath = intent.getStringExtra("video_path");
        u_id = intent.getStringExtra("u_id");
        titleStr = intent.getStringExtra("titleStr");
        video_title = findViewById(R.id.tv_course_id);
        video_title.setText(titleStr);



        videoView = (VideoView)findViewById(R.id.course_video_view); // 아이디로 찾는 거임
        //
      //  videoView.setVideoPath("https://shinple.s3.ap-northeast-2.amazonaws.com/course_vid_xp1EcP");
//                 videoView.setVideoURL(url);
        videoView.setVideoPath(videoPath);
        //
        final MediaController mediaController = new MediaController(this);
        videoView.setMediaController(mediaController);
        videoView.start();
        

        videoView.postDelayed(new Runnable() {
            @Override
            public void run() {
                mediaController.show(0);
                videoView.pause();
            }
        }, 100);


        note = (TextInputEditText)findViewById(R.id.note);
        note.setVisibility(View.GONE);
        note.setAlpha(0.5f);
        note_sw = (Switch)findViewById(R.id.note_on);

        note_sw.setOnCheckedChangeListener(new OnCheckedChangeListener() {

            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked){ // 강의 노트 스위치 함수
                if (isChecked){
                       note.setVisibility(View.VISIBLE);

                }else{
                        note.setVisibility(View.GONE);

                }

            }
        });

        note_sb = (SeekBar)findViewById(R.id.note_seekbar);
        note_sb.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() { // Seek Bar 조절 함수
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                note.setAlpha(((float)progress/100));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {


            }
        });
        //setOnclickListenr



        //tab, viewpager
        viewPager = findViewById(R.id.video_view);
        tabLayout = findViewById(R.id.video_tab);


        tabLayout.addTab(tabLayout.newTab().setText("강의목록"));
        tabLayout.addTab(tabLayout.newTab().setText("강의노트"));
        tabLayout.setSelectedTabIndicatorColor(Color.parseColor("#ffffff"));


        com.example.shinple.Adapter.VideoPagerAdapter pagerAdapter = new com.example.shinple.Adapter.VideoPagerAdapter(getSupportFragmentManager(), 2);
        viewPager.setAdapter(pagerAdapter);

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));


//            VideoFragmentOne fragment = new VideoFragmentOne();
//            Bundle bundle = new Bundle();
//            bundle.putString("c_id", c_id);
//            System.out.println("이 개같은 c_id는 : " + c_id + "입니다.");
//            bundle.putString("v_id", v_id);
//            bundle.putString("video_path", videoPath);
//            bundle.putString("u_id", u_id);
//            fragment.setArguments(bundle);
//



    }//onCreate Method End

    public String getV_id(){
        return this.v_id;
    }

    public String getU_id(){
        return this.u_id;
    }

    public void settingChanged(String v_id , String videoPath){
        this.v_id = v_id;
        this.videoPath=videoPath;
    }

    public String getC_id(){
        return this.c_id;
    }





    //노트 저장 버튼 구현
    public void onClick_nSave(View v){
        String note_content = note.getText().toString();
        Response.Listener<String> responseListener = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try{
                    JSONObject jsonResponse = new JSONObject(response);
                    boolean save_success = jsonResponse.getBoolean("save_success");
                    if (save_success){ // 성공시
                        Toast.makeText(getApplicationContext(),"노트를 등록 했습니다",Toast.LENGTH_SHORT).show();
                        note.setText(null);
                    }else{//실패시
                        Toast.makeText(getApplicationContext(),"노트를 등록하지 못했습니다",Toast.LENGTH_SHORT).show();
                    }
                }catch (JSONException e){
                    e.printStackTrace();
                }
            }
        };
        //나중에 매개변수 id들 다 변수로 해줘야됨
        NoteSaveRequest noteSaveRequest = new NoteSaveRequest(u_id,note_content,c_id,v_id,responseListener);
        RequestQueue queue = Volley.newRequestQueue(VideoActivity.this);
        queue.add(noteSaveRequest);
    }// 노트 저장 함수 끝


    //-------1. 추후 비디오 경로 변수로 넣어야함 서버에서 받아서--------
    @Override public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        int current_time = videoView.getCurrentPosition();
        boolean note_checked = note_sw.isChecked();
        float Alpha = note.getAlpha();
        String previous_note = note.getText().toString();

        if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) { // 가로화면 일때
            setContentView(R.layout.activity_video);
            videoView = (VideoView)findViewById(R.id.course_video_view); // 아이디로 찾는 거임
            videoView.setVideoPath(videoPath);
//                 videoView.setVideoURL(url);
            //
            final MediaController mediaController = new MediaController(this);
            videoView.setMediaController(mediaController);
            videoView.seekTo(current_time);
            videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mediaPlayer) {

                    // Set media player on seek complete listener.
                    mediaPlayer.setOnSeekCompleteListener(new MediaPlayer.OnSeekCompleteListener() {

                        // This method will be invoked when seekto method complete.
                        @Override
                        public void onSeekComplete(MediaPlayer mediaPlayer) {

                            // Play video only when seekto method complete and video is paused.
                            if(!(videoView.isPlaying()))
                            {
                                videoView.start();

                            }
                        }
                    });
                }
            });

        } else if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT){//세로화면 일때
            setContentView(R.layout.activity_video);
            videoView = (VideoView)findViewById(R.id.course_video_view); // 아이디로 찾는 거임

            videoView.setVideoPath(videoPath);
//
            final MediaController mediaController = new MediaController(this);
            videoView.setMediaController(mediaController);
            //videoView.start();
            videoView.seekTo(current_time);

            videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mediaPlayer) {

                    // Set media player on seek complete listener.
                    mediaPlayer.setOnSeekCompleteListener(new MediaPlayer.OnSeekCompleteListener() {

                        // This method will be invoked when seekto method complete.
                        @Override
                        public void onSeekComplete(MediaPlayer mediaPlayer) {

                            // Play video only when seekto method complete and video is paused.
                            if(!(videoView.isPlaying()))
                            {
                                videoView.start();



                            }
                        }
                    });
                }
            });

            note = (TextInputEditText)findViewById(R.id.note);
            if(note_checked)
                note.setVisibility(View.VISIBLE);
            else
                note.setVisibility(View.GONE);
            note.setVisibility(View.GONE);
            note.setAlpha(Alpha);
            note.setText(previous_note);
            note_sw = (Switch)findViewById(R.id.note_on);
            note_sw.setOnCheckedChangeListener(new OnCheckedChangeListener() {

                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked){ // 강의 노트 스위치 함수
                    if (isChecked){
                        note.setVisibility(View.VISIBLE);

                    }else{
                        note.setVisibility(View.GONE);

                    }

                }
            });

            note_sb = (SeekBar)findViewById(R.id.note_seekbar);
            note_sb.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() { // Seek Bar 조절 함수
                @Override
                public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                    note.setAlpha(((float)progress/100));
                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {

                }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {


                }
            });

        }

        // 노트 가져오기 전에 세로 방향 일때 가져올수 있는 값 다 가져오기

//        note = (TextInputEditText) findViewById(R.id.note);
//        note.setText(text);

    }









}

package com.example.shinple;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.shinple.Adapter.RecyclerAdapter;
import com.example.shinple.App_DB.RestClient;
import com.example.shinple.Course.CourseContents;
import com.example.shinple.Course.CourseDetailActivity;
import com.example.shinple.Course.VideoActivity;
import com.example.shinple.MyClass.MyClassRoom;
import com.example.shinple.MyClass.VideoListActivity;
import com.example.shinple.MyClass.qna_write;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.sql.Array;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;


public class MainFragmentOne extends Fragment {
    //ProgressBar pb1,pb2,pb3;
    RecyclerAdapter adapter;


    TextView[] textResponse = new TextView[6];  //원하는 TextView 크기 지정
    String[] text_column = new String[6];
    RestClient restClient; //RestClient class를 이용해야함 필요 시 만들어서 사용

    private Handler mHandler = new Handler(Looper.getMainLooper());

    TextView course_title;
    TextView course_instructor;
    TextView course_view_count;
    ImageView course_image_view;

    View view;

    TextView user_course_name;
    TextView learning_period;
    TextView leftDays;
    TextView noteNum;
    TextView myStatus;

    ProgressBar pb1;
    ProgressBar pb2;
    ProgressBar pb3;

    //fragment 안에서 인텐트 넘기기
    String u_id;
    List<String> course_id_array;
    List<String> course_subcategory_array;
    List<String> course_title_array;
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.main_menu_fragmnet1, container, false);

//        u_id = getActivity().getIntent().getStringExtra("u_id");

        Intent intent = getActivity().getIntent();
        u_id = intent.getStringExtra("u_id");

        //pb1 = (ProgressBar)view.findViewById(R.id.progressBar1);
        pb2 = (ProgressBar)view.findViewById(R.id.progressBar2);
        //pb3 = (ProgressBar)view.findViewById(R.id.progressBar3);

        //pb1.getProgressDrawable().setColorFilter(Color.GREEN, PorterDuff.Mode.SRC_IN);
        //pb1.setIndeterminate(false);

        //pb2.setIndeterminate(false);
        pb2.setMax(100);

//        pb3.setIndeterminate(false);
//        pb3.setMax(100);
//        pb3.setProgress(0);

        //메인 나의 학습 현황
        user_course_name = (TextView) view.findViewById(R.id.user_course_name);
        learning_period = (TextView) view.findViewById(R.id.learning_period);
        //leftDays = (TextView) view.findViewById(R.id.leftDays);
        //noteNum = (TextView) view.findViewById(R.id.noteNum);
        myStatus = (TextView) view.findViewById(R.id.myStatus);

        TimerTask task = new TimerTask() {
            @Override
            public void run() {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        restClient = RestClient.getInstance();
                        //                restClient.parseDB("https://shinple.kr/app_db/calculate_course.php?u_id="+u_id);
                        setListeners_top(restClient);
                    }
                });
            }
        };
        new Timer().schedule(task, 1000);

        //------------------> 새로 작성한 부분
        SearchView searchView = (SearchView)getActivity().findViewById(R.id.search_view);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener(){
            @Override
            public boolean onQueryTextSubmit(String query) {

                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                RecyclerAdapter result_adapter = new RecyclerAdapter();
                ArrayList<MainRecyclerData> data_list = adapter.getListData();
                for(int i = 0 ; i<data_list.size();i++){
                    if(data_list.get(i).getTitle().toLowerCase().contains(query.toLowerCase()))
                        result_adapter.addItem(data_list.get(i));

                }
                result_adapter.notifyDataSetChanged();
                RecyclerView recyclerView = view.findViewById(R.id.recyclerView);
                recyclerView.setAdapter(result_adapter);
                return true;
            }
        });




        //--------------------------->새로작성한 부분 끝


        ///end of 나의 학습 현황

        //하단 리사이클러뷰
//        init(view);
        //end of 하단 리사이클러뷰
        return view;

    } //----------------------->OnCreateView End
    // SSL 통신 시 필요 : DB 추가 부분 ------------------------------------------------------------------------------------------------------------------------


    private void init(View view) {
        getViews(view);
        restClient = RestClient.getInstance();
        restClient.parseDB("https://shinple.kr/app_db/course_tbl.php");  // 불러오고 싶은 DB 테이블 설정!!
        setListeners();
    }

    private void getViews(View view) {
        textResponse[0] = (TextView) view.findViewById(R.id.main_bottom_course_title);
        textResponse[1] = (TextView) view.findViewById(R.id.main_bottom_instructor);
        textResponse[2] = (TextView) view.findViewById(R.id.main_bottom_view_count);
        course_image_view = (ImageView) view.findViewById(R.id.main_bottom_image_view);

        text_column[0] = "c_name";
        text_column[1] = "instructor";
        text_column[2] = "view_count";
        text_column[3] = "thumbnail";
        text_column[4] = "c_id";
        text_column[5] = "sub_cate_name";
    }

    private void setListeners() {
        // request에 따른 response 함수
        new Thread(new Runnable() {
            @Override
            public void run() {
                String response = restClient.postRequest(); // response 내용 저장
                setText(response, textResponse); //파씽 함수 고고
                System.out.println(response);
            }
        }).start();
    }

    private void setListeners_top(final RestClient restClient) {
        // request에 따른 response 함수
        new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("u_id : "+u_id);
                String response = restClient.postRequest("https://shinple.kr/app_db/calculate_course.php?u_id="+u_id); // response 내용 저장
                System.out.println("resonse : "+response);
//                여기 바꾸기
                try {
                    JSONObject json = new JSONObject(response);
                    JSONArray arr = json.getJSONArray("user_data");
                    JSONObject json2;

                    // json -> VO
                    ArrayList<MainHeaderVO> listHeaderData = new ArrayList<>();
                    for(int i = 0; i < arr.length(); i++){
                        json2 = arr.getJSONObject(i);
                        MainHeaderVO vo = MainHeaderVO.from(json2);
                        listHeaderData.add(vo);
                    }

                    // 만료일 계산
                    int minLeftDays = Integer.MAX_VALUE;
                    MainHeaderVO targetVo = null;
                    for (MainHeaderVO vo : listHeaderData) {
                        if (vo.leftDays < minLeftDays) {
                            targetVo = vo;
                            minLeftDays = vo.leftDays;
                        }
                    }

                    final MainHeaderVO finalTargetVo = targetVo;
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            String learning_period_text ="학습시작일 : "+ finalTargetVo.startDate;
                            learning_period.setText(learning_period_text);

                            // 강의 이름
                            user_course_name.setText(finalTargetVo.name);

                            // 남은 학습일
//                            int leftDays_cal = finalTargetVo.leftDays;
//                            leftDays.setText(String.valueOf(leftDays_cal) + "일");
//                            pb1.setMax(100);
//                            pb1.setProgress(leftDays_cal);

                            // 진도율
                            float percentage = ((float) finalTargetVo.completedCnt/(float) finalTargetVo.totalCnt)*100;
                            myStatus.setText(String.format("%d", (int) percentage)+"%");
                            pb2.setProgress((int)percentage);

                            //노트갯수 totalCnt:7, completedCnt:3


                            //클릭시 해당 강의로 이동
                            ImageButton btn = (ImageButton) view.findViewById(R.id.main_course_btn);
                            btn.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    Intent intent = new Intent(getActivity(), VideoListActivity.class);
                                    intent.putExtra("u_id", Integer.parseInt(u_id));
                                    intent.putExtra("courseId", finalTargetVo.cid );
                                    intent.putExtra("subCategory", finalTargetVo.subCateName);
                                    intent.putExtra("title", finalTargetVo.name);
                                    startActivity(intent);
                                }
                            });

                        }
                    });
                }
                catch(Exception e){
                    e.printStackTrace();
                }
                System.out.println(response);
            }
        }).start();
    }


    private void setListeners_chk_course_takes(final int p) {
        // request에 따른 response 함수
        new Thread(new Runnable() {
            @Override
            public void run() {
                String response = restClient.postRequest(); // response 내용 저장

//                여기 바꾸기
                try {
                    System.out.println("response : " + response);
                    JSONObject json = new JSONObject(response);
                    JSONArray arr = json.getJSONArray("result");
                    JSONObject json2;



                    for(int i = 0; i < arr.length(); i++){
                        json2 = arr.getJSONObject(i);

                        System.out.println(json2.getString("c_id").equals("none"));

                        if(json2.getString("c_id").equals("none")){
                            Intent intent = new Intent(getActivity(), CourseDetailActivity.class);
//                            System.out.println(Integer.parseInt(course_id_array.get(p)));
//                            System.out.println(course_subcategory_array.get(p));
//                            System.out.println(course_title_array.get(p));
                            intent.putExtra("u_id", Integer.parseInt(u_id));
                            intent.putExtra("courseId", Integer.parseInt(course_id_array.get(p)));
                            intent.putExtra("subCategory", course_subcategory_array.get(p));
                            intent.putExtra("title", course_title_array.get(p));
                            startActivity(intent);
                            break;
                        }else{
                            System.out.println("check - listActivity");
                            Intent intent = new Intent(getActivity(), VideoListActivity.class);
                            intent.putExtra("u_id", Integer.parseInt(u_id));
                            intent.putExtra("courseId", Integer.parseInt(course_id_array.get(p)));
                            intent.putExtra("subCategory", course_subcategory_array.get(p));
                            intent.putExtra("title", course_title_array.get(p));
                            startActivity(intent);
                            break;
                        }
                    }
                }
                catch(Exception e){
                    e.printStackTrace();
                }
                System.out.println(response);
            }
        }).start();
    }
    //.runOnUiThread
    private void setText(final String response, final TextView[] textRes) {
        mHandler.post(new Runnable() {
            @Override


            public void run() {

                RecyclerView recyclerView = view.findViewById(R.id.recyclerView);

                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
                recyclerView.setLayoutManager(linearLayoutManager);

                adapter = new RecyclerAdapter();
                recyclerView.setAdapter(adapter);

                adapter.setOnItemClickListener(new RecyclerAdapter.OnItemClickListener() {
                    @Override
                    public void onItemClick(View v, int position) {
                        // TODO : 아이템 클릭 이벤트를 MainActivity에서 처리.
                        restClient = RestClient.getInstance();
                        restClient.parseDB("https://shinple.kr/app_db/chk_takes_tbl.php?u_id="+u_id+"&"+"c_id="+course_id_array.get(position));
                        setListeners_chk_course_takes(position);


//                        Toast.makeText(getActivity(), course_id_array.get(position)+"", Toast.LENGTH_SHORT).show();
                    }
                });

                try {
                    JSONObject json = new JSONObject(response);
                    JSONArray arr = json.getJSONArray("result");

                    List<String> title = new ArrayList<String>();
                    List<String> instructor = new ArrayList<String>();
                    List<String> view_count = new ArrayList<String>();
                    List<String> course_thumb_array = new ArrayList<String>();
                    course_id_array = new ArrayList<>();
                    course_subcategory_array = new ArrayList<>();
                    course_title_array = new ArrayList<>();

                    JSONObject json2;

                    for(int i = 0; i < arr.length(); i++){
                        json2 = arr.getJSONObject(i);
                        String course_title = json2.getString(text_column[0]);
                        String course_instructor = json2.getString(text_column[1]);
                        String course_view_count = json2.getString(text_column[2]);
                        String course_image_thumb = json2.getString(text_column[3]);
                        String course_id = json2.getString(text_column[4]);
//                        String course_subcategory = json.getString("sub_cate_name");
                        String course_subcategory = json2.getString(text_column[5]);

                        title.add(course_title);
                        instructor.add(course_instructor);
                        view_count.add(course_view_count);
                        course_thumb_array.add(course_image_thumb);
                        course_id_array.add(course_id);
                        course_subcategory_array.add(course_subcategory);
                        course_title_array.add(course_title);
                    }

                    for (int i = 0; i < arr.length(); i++) {
                        // 각 List의 값들을 mainRecyclerData 객체에 set 해줍니다.
                        MainRecyclerData mainRecyclerData = new MainRecyclerData();
                        mainRecyclerData.setTitle(title.get(i));
                        mainRecyclerData.setContent(instructor.get(i));
                        mainRecyclerData.setImage_thumb(course_thumb_array.get(i), course_image_view);

                        // 각 값이 들어간 data를 adapter에 추가합니다.
                        adapter.addItem(mainRecyclerData);
                    }
                    // adapter의 값이 변경되었다는 것을 알려줍니다.
                    adapter.notifyDataSetChanged();

                }
                catch(Exception e){
                    e.printStackTrace();
                }
            }
        });
    }
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init(view);
    }

    static class MainHeaderVO {
        String name;
        String startDate;
        String endDate;
        String subCateName;
        int cid;
        int totalCnt;
        int completedCnt;
        int leftDays;

        public MainHeaderVO(String name, String startDate, String endDate, String subCateName, int cid, int leftDays, int totalCnt, int completedCnt) {
            this.name = name;
            this.startDate = startDate;
            this.endDate = endDate;
            this.cid=cid;
            this.subCateName=subCateName;
            this.leftDays = leftDays;
            this.totalCnt = totalCnt;
            this.completedCnt = completedCnt;
        }

        public static MainHeaderVO from(JSONObject json2) throws JSONException, ParseException {
            String name = json2.getString("c_name");
            String start_date = json2.getString("start_date");
            String end_date = json2.getString("end_date");
            String subCateName = json2.getString("sub_cate_name");
            int totalCnt = json2.getInt("totalCnt");
            int completedCnt = json2.getInt("completedCnt");
            int cid = json2.getInt("c_id");

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            java.util.Date endRawDate = sdf.parse(end_date);

            Calendar todayDate = Calendar.getInstance(); // 오늘 날짜.

            Calendar startDate = Calendar.getInstance(); // 시작하는 날짜.
            startDate.setTime(endRawDate);

            long timeOffset = todayDate.getTimeInMillis()-startDate.getTimeInMillis();
            int leftDays = 0;
            if (timeOffset > 0) {
                leftDays = (int) (timeOffset / DateUtils.DAY_IN_MILLIS);
            }

            return new MainHeaderVO(name, start_date, end_date, subCateName, cid, leftDays, totalCnt, completedCnt);
        }
    }
}

package com.example.shinple.Adapter;


public class Qna {
    String qna;
    String name;
    String date;

    public Qna(String qna, String name, String date ){
        this.name = name;
        this.date = date;
        this.qna = qna;
    }

    public String getQna() {
        return qna;
    }

    public void setNotice(String qna) { this.qna = qna; }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}

package com.example.shinple.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.shinple.Course.CourseDetailActivity;
import com.example.shinple.Course.WholeCourse;
import com.example.shinple.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
/*Q&A 게시판 Adapter입니다.*/

public class FullCourseAdapter extends BaseAdapter {


    String u_id = "1";

    WholeCourse listViewItem;

    // Adapter에 추가된 데이터를 저장하기 위한 ArrayList
    private ArrayList<WholeCourse> listViewItemList = new ArrayList<WholeCourse>();
//    ImageView thumbnail_viewholder;

    private AdapterView.OnItemClickListener mListener = null ;

    // ListViewAdapter의 생성자
    public FullCourseAdapter() {

    }

    public interface OnItemClickListener {
        void onItemClick(View v, int position) ;
    }

    // Adapter에 사용되는 데이터의 개수를 리턴. : 필수 구현
    @Override
    public int getCount() {
        return listViewItemList.size();
    }

    // position에 위치한 데이터를 화면에 출력하는데 사용될 View를 리턴. : 필수 구현
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        final int pos = position;
        final Context context = parent.getContext();

        // "listview_item" Layout을 inflate하여 convertView 참조 획득.
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.course_learining, parent, false);
        }

        // 화면에 표시될 View(Layout이 inflate된)으로부터 위젯에 대한 참조 획득
//        ImageView iconImageView = (ImageView) convertView.findViewById(R.id.courseimage) ;
        TextView cateTextView = (TextView) convertView.findViewById(R.id.course_sub_category);
        TextView titleTextView = (TextView) convertView.findViewById(R.id.coursetitle);
        TextView instructorTextView = (TextView) convertView.findViewById(R.id.courseinstructor);

        ImageView thumbnail_viewholder = (ImageView) convertView.findViewById(R.id.courseimage);

        // Data Set(listViewItemList)에서 position에 위치한 데이터 참조 획득
        listViewItem = listViewItemList.get(position);

//        //리스트뷰 클릭 이벤트
        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, CourseDetailActivity.class);

                intent.putExtra("u_id", Integer.parseInt(u_id));
                intent.putExtra("courseId", Integer.parseInt(listViewItem.getCourse_id()));
                intent.putExtra("subCategory", listViewItem.getCategory());
                intent.putExtra("title", listViewItem.getTitle());

                System.out.println(u_id+listViewItem.getCourse_id()+ listViewItem.getCategory()+listViewItem.getTitle());

                context.startActivity(intent);

//                Toast.makeText(context ,pos+"번째",Toast.LENGTH_SHORT).show();
            }
        });



        // 아이템 내 각 위젯에 데이터 반영
        cateTextView.setText(listViewItem.getCategory());
        titleTextView.setText(listViewItem.getTitle());
        instructorTextView.setText(listViewItem.getInstructor());
        Picasso.with(thumbnail_viewholder.getContext())
                .load(listViewItem.getThumbnail())
                .into(thumbnail_viewholder);

        return convertView;
    }

    private static class ViewHolder {
        ImageView mImageView;
    }

    // 지정한 위치(position)에 있는 데이터와 관계된 아이템(row)의 ID를 리턴. : 필수 구현
    @Override
    public long getItemId(int position) {
        return position;
    }

    // 지정한 위치(position)에 있는 데이터 리턴 : 필수 구현
    @Override
    public Object getItem(int position) {
        return listViewItemList.get(position);
    }

    // 아이템 데이터 추가를 위한 함수. 개발자가 원하는대로 작성 가능.
    public void addItem(String category, String title, String instructor, String thumbnail, String course_id) {
        WholeCourse item = new WholeCourse();

        item.setCategory(category);
        item.setTitle(title);
        item.setInstructor(instructor);
        item.setThumbnail(thumbnail);
        item.setCourse_id(course_id);

        listViewItemList.add(item);

    }
}

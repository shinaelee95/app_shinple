package com.example.shinple.Adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import android.widget.Toast;
import com.example.shinple.ListVO.NoticeListVO;
import com.example.shinple.Notice.NoticeActivity;
import com.example.shinple.Notice.NoticeDetail;
import com.example.shinple.R;
import java.util.ArrayList;
import java.util.List;


import static androidx.core.content.ContextCompat.startActivity;

public class NoticeListViewAdapter extends BaseAdapter {
//    List<Integer> use_n_id;

    private ArrayList<NoticeListVO> listVO = new ArrayList<NoticeListVO>();

    public NoticeListViewAdapter() {
    }

//    public void set_u_id(List<Integer> n_id){
//        use_n_id=n_id;
//    }

    @Override
    public int getCount() {
        return listVO.size();
    }

    // ** 이 부분에서 리스트뷰에 데이터를 넣어줌 **
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        //postion = ListView의 위치      /   첫번째면 position = 0
        final int pos = position;
        final Context context = parent.getContext();


        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.notice_custom, parent, false);
        }

        TextView title = (TextView) convertView.findViewById(R.id.title_text);
        TextView created_date = (TextView) convertView.findViewById(R.id.created_date_text);
        TextView admin_id= (TextView) convertView.findViewById(R.id.admin_id_text);
        TextView view_count = (TextView) convertView.findViewById(R.id.view_count_text);
        NoticeListVO listViewItem = listVO.get(position);


        // 아이템 내 각 위젯에 데이터 반영
        title.setText(listViewItem.getTitle());
        created_date.setText(listViewItem.getCreated_date());
        admin_id.setText(listViewItem.getAdmin_id());
        view_count.setText(listViewItem.getView_count());


        //리스트뷰 클릭 이벤트
//        convertView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Toast.makeText(context, (pos + 1) + "번째 공지사항 선택", Toast.LENGTH_SHORT).show();
//                // 사용법
//                for(int i =0; i<use_n_id.size(); i++){
//                    System.out.println(use_n_id.get(i)); //출력대신 사용하면됨!!
//                }
//            }
//        });

        return convertView;
    }
    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public Object getItem(int position) {
        return listVO.get(position);
    }

    // 데이터값 넣어줌
    public void addVO(String title, String created_date, String admin_id, String view_count, int n_id) {
        NoticeListVO item = new NoticeListVO();
        item.setTitle(title);
        item.setCreated_date(created_date);
        item.setAdmin_id(admin_id);
        item.setView_count(view_count);
        item.setN_id(n_id);
        listVO.add(item);
    }
}
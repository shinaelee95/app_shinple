package com.example.shinple.Adapter;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.example.shinple.Course.VideoFragmentOne;
import com.example.shinple.Course.VideoFragmentTwo;

/* 이미지 슬라이더 Adapter 입니다. */
public class VideoPagerAdapter extends FragmentStatePagerAdapter {


    private int tabCount;

    public VideoPagerAdapter(FragmentManager fragmentManager, int tabCount) {
        super(fragmentManager);
        this.tabCount = tabCount;
    }

    public VideoPagerAdapter(FragmentManager supportFragmentManager) {
        super(supportFragmentManager);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0: {
                VideoFragmentOne videoFragmentOne = new VideoFragmentOne();
                return videoFragmentOne;
            }
            case 1: {
                VideoFragmentTwo videoFragmentTwo = new VideoFragmentTwo();
                return videoFragmentTwo;
            }

            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return tabCount;
    }


}
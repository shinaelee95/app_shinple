package com.example.shinple.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import android.widget.Toast;
import com.example.shinple.ListVO.NoteListVO;
import com.example.shinple.Notice.NoticeDetail;
import com.example.shinple.R;
import java.util.ArrayList;

public class NoteListViewAdapter extends BaseAdapter {

    private ArrayList<NoteListVO> listVO = new ArrayList<NoteListVO>();

    public NoteListViewAdapter() {
    }

    @Override
    public int getCount() {
        return listVO.size();
    }

    // ** 이 부분에서 리스트뷰에 데이터를 넣어줌 **
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        //postion = ListView의 위치      /   첫번째면 position = 0
        final int pos = position;
        final Context context = parent.getContext();


        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.note_custom, parent, false);
        }

        TextView c_name = (TextView) convertView.findViewById(R.id.c_name_text);
        TextView v_title = (TextView) convertView.findViewById(R.id.v_title_text);
        TextView created_date = (TextView) convertView.findViewById(R.id.created_date_text);
        NoteListVO listViewItem = listVO.get(position);


        // 아이템 내 각 위젯에 데이터 반영
        c_name.setText(listViewItem.getC_name());
        v_title.setText(listViewItem.getV_title());
        created_date.setText(listViewItem.getCreated_date());


        //리스트뷰 클릭 이벤트
//        convertView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Toast.makeText(context, (pos + 1) + "번째 강의노트 선택", Toast.LENGTH_SHORT).show();
//            }
//        });

        return convertView;
    }


    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public Object getItem(int position) {
        return listVO.get(position);
    }

    // 데이터값 넣어줌
    public void addVO(String c_name, String v_title, String created_date, int n_id) {
        NoteListVO item = new NoteListVO();
        item.setC_name(c_name);
        item.setV_title(v_title);
        item.setCreated_date(created_date);
//        item.setC_id(c_id);
////        item.setV_id(v_id);
        item.setN_id(n_id);
        listVO.add(item);
    }
}
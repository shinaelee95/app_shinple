package com.example.shinple.Adapter;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.example.shinple.MainFragmentOne;
import com.example.shinple.MainFragmentThree;
import com.example.shinple.MainFragmentTwo;


public class MainPagerAdapter extends FragmentStatePagerAdapter {


    private int tabCount;

    public MainPagerAdapter(FragmentManager fragmentManager, int tabCount) {
        super(fragmentManager);
        this.tabCount = tabCount;
    }

    public MainPagerAdapter(FragmentManager supportFragmentManager) {
        super(supportFragmentManager);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0: {
                MainFragmentOne mainFragmentOne = new MainFragmentOne();
                return mainFragmentOne;
            }
            case 1: {
                MainFragmentTwo mainFragmentTwo = new MainFragmentTwo();
                return mainFragmentTwo;
            }
            case 2: {
                MainFragmentThree mainFragmentThree = new MainFragmentThree();
                return mainFragmentThree;
            }

            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return tabCount;
    }


}
package com.example.shinple.Adapter;

public class Note {

    public String title;
    public String date;

    public Note(String title, String date ){
        this.title = title;
        this.date = date;


    }

    public String getNote() {
        return title;
    }

    public String getTitle() {
        return title;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}

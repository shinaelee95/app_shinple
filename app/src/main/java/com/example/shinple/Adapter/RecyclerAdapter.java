package com.example.shinple.Adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.shinple.MainRecyclerData;
import com.example.shinple.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.ItemViewHolder> {

    // adapter에 들어갈 list 입니다.
    public ArrayList<MainRecyclerData> listData = new ArrayList<>();
    // 리스너 객체 참조를 저장하는 변수
    private OnItemClickListener mListener = null ;

    public ArrayList<MainRecyclerData> getListData(){
        return listData;
    }

    public void setListData(ArrayList<MainRecyclerData> listData){
        this.listData = listData;
    }


    public interface OnItemClickListener {
        void onItemClick(View v, int position) ;
    }
    // OnItemClickListener 리스너 객체 참조를 어댑터에 전달하는 메서드
    public void setOnItemClickListener(OnItemClickListener listener) {
        this.mListener = listener ;
    }
    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        // LayoutInflater를 이용하여 전 단계에서 만들었던 item_mainbottom.xml을 inflate 시킵니다.
        // return 인자는 ViewHolder 입니다.
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_mainbottom, parent, false);
        return new ItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ItemViewHolder holder, int position) {
        // Item을 하나, 하나 보여주는(bind 되는) 함수입니다.
        holder.onBind(listData.get(position));
    }

    @Override
    public int getItemCount() {
        // RecyclerView의 총 개수 입니다.
        return listData.size();
    }

    public void addItem(MainRecyclerData mainRecyclerData) {
        // 외부에서 item을 추가시킬 함수입니다.
        listData.add(mainRecyclerData);
    }

    // RecyclerView의 핵심인 ViewHolder 입니다.
    // 여기서 subView를 setting 해줍니다.
    public class ItemViewHolder extends RecyclerView.ViewHolder {

        private TextView textView1;
        private TextView textView2;
        private TextView textView3;
        private ImageView course_thumb_view_holder;


        ItemViewHolder(View itemView) {
            super(itemView);

            textView1 = itemView.findViewById(R.id.main_bottom_course_title);
            textView2 = itemView.findViewById(R.id.main_bottom_instructor);
//            textView3 = itemView.findViewById(R.id.course_period);
            course_thumb_view_holder = itemView.findViewById(R.id.main_bottom_image_view);

            itemView.setOnClickListener(new View.OnClickListener(){

                @Override
                public void onClick(View view) {
                    int pos = getAdapterPosition() ;
                    if (pos != RecyclerView.NO_POSITION) {
                        // 리스너 객체의 메서드 호출.
                        if (mListener != null) {
                            mListener.onItemClick(view, pos) ;
                        }
                    }
                }
            });

        }

        public void onBind(MainRecyclerData mainRecyclerData) {
            textView1.setText(mainRecyclerData.getTitle());
            textView2.setText(mainRecyclerData.getContent());
//            textView3.setText(mainRecyclerData.getCourse_period());
            Picasso.with(course_thumb_view_holder.getContext())
            .load(mainRecyclerData.getImage_thumb())
            .into(course_thumb_view_holder);

        }
    }
}

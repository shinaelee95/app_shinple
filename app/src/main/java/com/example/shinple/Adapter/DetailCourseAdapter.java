package com.example.shinple.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.example.shinple.ListVO.DetailCourseVO;
import com.example.shinple.ListVO.VideoListVO;
import com.example.shinple.R;

import java.util.ArrayList;

public class DetailCourseAdapter extends BaseAdapter {
    private Context context;
    private int value;
    DetailCourseVO detailData =  new DetailCourseVO();
    private ArrayList<VideoListVO> videolist = new ArrayList<VideoListVO>();

    public DetailCourseAdapter() {
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @Override
    public int getCount() {
        return videolist.size();
    }

    @Override
    public Object getItem(int i) {
        return videolist.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {

        System.out.println("convert : " + convertView);

        final int pos = position;
        final Context context = viewGroup.getContext();

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.activity_course_detail, viewGroup, false);
        }

        TextView tv_course_contents_title =(TextView)convertView.findViewById(R.id.tv_course_contents_title);
        ImageView course_thumb_detail_image = (ImageView) convertView.findViewById(R.id.course_thumb_detail_image);
        TextView tv_course_contents = (TextView) convertView.findViewById(R.id.tv_course_contents);

        tv_course_contents_title.setText(detailData.getTitle());
        tv_course_contents.setText(detailData.getCourseInfo());

        return convertView;
    }

    public void addCourseItem(String courseId, String subCategory, String title,  String thumbnail, String courseInfo,String instructor, String viewCount ,String period) {
        detailData.setCourseId(Integer.parseInt( courseId));
        detailData.setSubCategory(subCategory);
        detailData.setTitle(title);
        detailData.setThumbnail(thumbnail);
        detailData.setCourseInfo(courseInfo);
        detailData.setInstructor(instructor);
        detailData.setTakesCnt(Integer.parseInt(viewCount));
        detailData.setPeriod(Integer.parseInt(period));
    }

    public void addVideoItem(String v_id, String title,  String total_time) {
        VideoListVO videoData = new VideoListVO();

        videoData.setTitle(title);
        videoData.setTotalTime(total_time);
        videolist.add(videoData);
    }
}


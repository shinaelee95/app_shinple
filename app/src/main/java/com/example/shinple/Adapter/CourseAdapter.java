//package com.example.shinple.Adapter;
//
//import android.content.Context;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.BaseAdapter;
//import android.widget.ImageView;
//import android.widget.TextView;
//
//import com.example.shinple.Course.WholeCourse;
//import com.example.shinple.R;
//import com.squareup.picasso.Picasso;
//
//import java.util.ArrayList;
///*강의실 게시판 Adapter입니다.*/
//
//public class CourseAdapter extends BaseAdapter {
//
//    private Context context;
//    private ArrayList<WholeCourse> wholeCoursesList;
//    private int value;
//
//    public CourseAdapter() {        //Context context, List<Learning> learninglist
//        this.context = context;
//        this.wholeCoursesList = wholeCoursesList;
//    }
//
//
//    public int getValue() {
//        return value;
//    }
//
//    public void setValue(int value) {
//        this.value = value;
//    }
//
//    @Override
//    public int getCount() {
//        return wholeCoursesList.size();
//    }
//
//    @Override
//    public Object getItem(int i) {
//        return wholeCoursesList.get(i);
//    }
//
//    @Override
//    public long getItemId(int i) {
//        return i;
//    }
//
//
//
//
//    @Override
//    public View getView(int i, View view, ViewGroup viewGroup) {
//        View v = View.inflate(context, R.layout.qna, null);
//
//        TextView coursetitle = (TextView) v.findViewById(R.id.qna_text);
//        TextView qnaCategory = (TextView) v.findViewById(R.id.qnacategory);
//        TextView qnadate = (TextView) v.findViewById(R.id.qnadate);
//
//        qnatext.setText(wholeCoursesList.get(i).getQna());
//        qnaCategory.setText(wholeCoursesList.get(i).get());
//        qnadate.setText(qnalist.get(i).getDate());
//
//        v.setTag(qnalist.get(i).getQna());
//        return v;
//
//    }
//}
//
//
//
//
////    @Override
////    public View getView(int position, View view, ViewGroup viewGroup) {
////        /*        if(view == null){
////            view = View.inflate(context, R.layout.learning_status, null);
////
////        }*/
////        final int pos = position;
////        final Context context = viewGroup.getContext();
////
////        if (view == null) {
////            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
////            view = inflater.inflate(R.layout.item_mainbottom, viewGroup, false);
////        }
////
////        TextView coursetitle = (TextView) view.findViewById(R.id.main_bottom_course_title);
////        TextView coursecategory = (TextView) view.findViewById(R.id.coursecategory);
////        //TextView learning_date = (TextView) view.findViewById(R.id.learning_date);
////        TextView courseinstructor = (TextView) view.findViewById(R.id.main_bottom_instructor);
////        //TextView progress_rate = (TextView) view.findViewById(R.id.progress_rate);
////        //ProgressBar progress_my = (ProgressBar) view.findViewById(R.id.progress_my);
////        TextView courseperiod = (TextView) view.findViewById(R.id.course_period);
////        ImageView coursethumb = (ImageView) view.findViewById(R.id.main_bottom_image_view);
////        WholeCourse wholeCourseListItem = wholeCoursesList.get(pos);
////
////        coursetitle.setText("[" + wholeCourseListItem.getCourestitle() + "]");                                    // 강좌 서브 카테고리 명
////        coursecategory.setText(wholeCourseListItem.getCoursecategory());// 강좌 타이틀
////        courseinstructor.setText(wholeCourseListItem.getCourseinstructor());
////        courseperiod.setText(wholeCourseListItem.getCourseperiod());
////        Picasso.with(coursethumb.getContext())
////                .load(wholeCourseListItem.getImage_thumb())
////                .into(coursethumb);
////
////
////        //learning_date.setText(learningListItem.getLearning_date());
////        //courseperiod.setText(wholeCourseListItem.getCourseperiod());        // 강좌 수강 기간 ( start_date ~ end_ date)
////        //progress_rate.setText(learninglist.get(i).getProgress_rate());
//////        int percentData = learningListItem.getProgress_rate();
//////        if(percentData == 100){
//////            progress_my.getProgressDrawable().setColorFilter(Color.GREEN, PorterDuff.Mode.SRC_IN);
//////        }
//////        else{
//////            progress_my.getProgressDrawable().setColorFilter(Color.BLUE, PorterDuff.Mode.SRC_IN);
//////        }
//////        progress_my.setProgress(percentData);                                                 // 강좌 진도율
//////        percent.setText(percentData+"%");
//////        // -> 이건 어떤 기능?
//////        //view.setTag(learningListItem.getLecture_subcategory());
////
////        return view;
////    }
////}
//
///*            view.setOnClickListener(new View.OnClickListener() {
//        @Override
//        public void onClick(View view) {
//            Intent intent = new Intent(context, CourseDetailActivity.class);
//            context.startActivity(intent);
//        }
//    });*/
//
//
////    //190828 민정 - 추가
////    public void addItem(int courseId, String subCategory,String thumbnail, String title,  String startDate, String endDate,String completedCnt ,String totalCnt, String status) {
////        LearnVO item = new LearnVO();
////
////        item.setCourse_id(courseId);
////        item.setLecture_subcategory(subCategory);
////        item.setLecture_name(title);
////        item.setStart_date(startDate);
////        item.setEnd_date(endDate);
////
////        // 강좌 진도율 계산
////        int comp_cnt = Integer.parseInt(completedCnt);
////        int total_cnt = Integer.parseInt(totalCnt);
////        int percent = 0;
////        if(comp_cnt > 0)
////        {
////            percent = (int)(((float)comp_cnt / (float)total_cnt) * 100);
////        }
////
////        item.setCompleted_Cnt(Integer.parseInt(completedCnt));
////        item.setTotal_Cnt(Integer.parseInt(totalCnt));
////        item.setProgress_rate(percent);
////
////        item.setLecture_status(Integer.parseInt(status));
////        item.setThumbnail(thumbnail);
////        learninglist.add(item);
////    }
////}
//
//
////190828 이영 -  마이페이지 - 수강한 강좌 리스트 목록 -  버튼 이벤트 주석처리
///*
//        final Button btn = (Button) v.findViewById(R.id.room_button);
////        btn.setOnClickListener(
////                new Button.OnClickListener() {
////                    public void onClick(View v) {
////                        Intent intent = new Intent(getActivity(),qna_write.class);
////                        getActivity().startActivity(intent);
////                    }
////                }
////        );
//        btn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent intent = new Intent(context, CourseDetailActivity.class);
//                context.startActivity(intent);
//            }
//        });
//
// */

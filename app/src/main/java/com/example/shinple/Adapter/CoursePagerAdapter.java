package com.example.shinple.Adapter;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.example.shinple.Course.CourseFragmentFour;
import com.example.shinple.Course.CourseFragmentOne;
import com.example.shinple.Course.CourseFragmentThree;
import com.example.shinple.Course.CourseFragmentTwo;

/* 이미지 슬라이더 Adapter 입니다. */
public class CoursePagerAdapter extends FragmentStatePagerAdapter {


    private int tabCount;

    public CoursePagerAdapter(FragmentManager fragmentManager, int tabCount) {
        super(fragmentManager);
        this.tabCount = tabCount;
    }

    public CoursePagerAdapter(FragmentManager supportFragmentManager) {
        super(supportFragmentManager);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0: {
                CourseFragmentOne courseFragmentOne = new CourseFragmentOne();
                return courseFragmentOne;
            }
            case 1: {
                CourseFragmentTwo courseFragmentTwo = new CourseFragmentTwo();
                return courseFragmentTwo;
            }
            case 2: {
                CourseFragmentThree courseFragmentThree = new CourseFragmentThree();
                return courseFragmentThree;
            }
            case 3: {
                CourseFragmentFour courseFragmentFour = new CourseFragmentFour();
                return courseFragmentFour;
            }

            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return tabCount;
    }


}
package com.example.shinple.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.shinple.ListVO.LearnVO;
import com.example.shinple.R;

import java.util.ArrayList;
/*강의실 게시판 Adapter입니다.*/

public class LearningStatus2Adapter extends BaseAdapter {

    private Context context;
    private ArrayList<LearnVO> learninglist = new ArrayList<LearnVO>();
    // List<Learning> learninglist;
    private int value;

    public LearningStatus2Adapter() {        //Context context, List<Learning> learninglist
//        this.context = context;
//        this.learninglist = learninglist;
    }


    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @Override
    public int getCount() {
        return learninglist.size();
    }

    @Override
    public Object getItem(int i) {
        return learninglist.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        /*        if(view == null){
            view = View.inflate(context, R.layout.learning_status, null);

        }*/
        final int pos = position;
        final Context context = viewGroup.getContext();

        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.course_learining, viewGroup, false);
        }

        TextView learning_category =(TextView)view.findViewById(R.id.learning_category);
        TextView lecture_name = (TextView) view.findViewById(R.id.lecture_name);
        ImageView thumbnail_view = (ImageView)view.findViewById(R.id.main_bottom_image_view);
        LearnVO learningListItem = learninglist.get(pos);

        learning_category.setText("[" + learningListItem.getLecture_subcategory() +"]" );                                    // 강좌 서브 카테고리 명
        lecture_name.setText(learningListItem.getLecture_name());                                                 // 강좌 타이틀



        return view;
    }


    //190828 민정 - 추가
    public void addItem(int courseId, String subCategory,String thumbnail, String title,  String startDate, String endDate,String completedCnt ,String totalCnt, String status) {
        LearnVO item = new LearnVO();

        item.setCourse_id(courseId);
        item.setLecture_subcategory(subCategory);
        item.setLecture_name(title);
        item.setStart_date(startDate);
        item.setEnd_date(endDate);

        item.setCompleted_Cnt(Integer.parseInt(completedCnt));
        item.setTotal_Cnt(Integer.parseInt(totalCnt));
        item.setLecture_status(Integer.parseInt(status));
        item.setThumbnail(thumbnail);
        learninglist.add(item);
}
}


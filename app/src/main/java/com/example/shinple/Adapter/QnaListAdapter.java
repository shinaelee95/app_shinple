package com.example.shinple.Adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.shinple.R;

import java.util.List;
/*Q&A 게시판 Adapter입니다.*/

public class QnaListAdapter extends BaseAdapter {

    private Context context;
    private List<Qna> qnalist;

    public QnaListAdapter(Context context, List<Qna> qnalist) {
        this.context = context;
        this.qnalist = qnalist;
    }

    @Override
    public int getCount() {
        return qnalist.size();
    }

    @Override
    public Object getItem(int i) {
        return qnalist.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View v = View.inflate(context, R.layout.qna, null);

        TextView qnatext = (TextView) v.findViewById(R.id.qna_text);
        TextView qnaCategory = (TextView) v.findViewById(R.id.qnacategory);
        TextView qnadate = (TextView) v.findViewById(R.id.qnadate);

        qnatext.setText(qnalist.get(i).getQna());
        qnaCategory.setText(qnalist.get(i).getName());
        qnadate.setText(qnalist.get(i).getDate());

        v.setTag(qnalist.get(i).getQna());
        return v;

    }
}

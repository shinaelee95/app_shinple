package com.example.shinple.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.INotificationSideChannel;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.shinple.ListVO.VideoListVO;
import com.example.shinple.R;

import java.util.ArrayList;

public class VideoListAdapter extends BaseAdapter {
    private ArrayList<VideoListVO> listViewItemList = new ArrayList<VideoListVO>();

    public VideoListAdapter(){

    }
    @Override
    public int getCount()
    {
        return listViewItemList.size();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        final int pos =position;
        final Context context = parent.getContext();

        if(convertView == null){
            LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.activity_video_custom_list, parent, false);
        }

        TextView titleTextView = (TextView) convertView.findViewById(R.id.title) ;
        TextView playTimeTextView = (TextView) convertView.findViewById(R.id.playTime) ;
        TextView totalTimeTextView = (TextView) convertView.findViewById(R.id.totalTime) ;
        TextView endDateTextView = (TextView) convertView.findViewById(R.id.endDate) ;
        TextView videoPercentTextView = (TextView) convertView.findViewById(R.id.videoPercent) ;
        ProgressBar progressBar = (ProgressBar)convertView.findViewById(R.id.videoProgressbar);


        VideoListVO videoListItem = listViewItemList.get(position);

        int percent = 0;

        try {
            String p_sec_str = videoListItem.getPlayTimeToSec();
            String t_sec_str = videoListItem.getTotalTimeToSec();

            double t_sec = Double.parseDouble(t_sec_str);
            double p_sec = Double.parseDouble(p_sec_str);


            if(p_sec > 0) {
                percent = (int)((p_sec / t_sec) *100);
        //        System.out.println("percent : " +percent);
            }
        }
        catch (Exception ex)
        {
            Log.e("Error Log : " , ex.toString());
        }

        // 아이템 내 각 위젯에 데이터 반영
        titleTextView.setText(videoListItem.getTitle());
        playTimeTextView.setText(videoListItem.getPlayTime());
        totalTimeTextView.setText(" / " + videoListItem.getTotalTime());
        progressBar.setProgress(percent);
        videoPercentTextView.setText(percent +"%");
        endDateTextView.setText(videoListItem.getEndDate());

        return convertView;
    }

    // 지정한 위치(position)에 있는 데이터와 관계된 아이템(row)의 ID를 리턴. : 필수 구현
    @Override
    public long getItemId(int position) {
        return position ;
    }

    // 지정한 위치(position)에 있는 데이터 리턴 : 필수 구현
    @Override
    public Object getItem(int position) {
        return listViewItemList.get(position) ;
    }

    // 아이템 데이터 추가를 위한 함수. 개발자가 원하는대로 작성 가능.
    //    public void addItem(Drawable thumbnail,  String title, String playTime, String totalTime) {
    public void addItem( String videoId,  String title,  String playSec, String totalSec, String playTime, String totalTime, String endDate, String videoPath) {
        VideoListVO item = new VideoListVO();
        item.setVideoId(Integer.parseInt(videoId));
        item.setTitle(title);
        item.setPlayTimeToSec(playSec);
        item.setTotalTimeToSec(totalSec);
        item.setPlayTime(playTime);
        item.setTotalTime(totalTime);
        item.setEndDate(endDate);
        item.setVideoPath(videoPath);

        listViewItemList.add(item);
    }
}

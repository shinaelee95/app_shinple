package com.example.shinple.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.shinple.Course.CourseDetailActivity;
import com.example.shinple.Course.VideoActivity;
import com.example.shinple.ListVO.ListVO;
import com.example.shinple.ListVO.VideoListVO;
import com.example.shinple.MainMenuActivity;
import com.example.shinple.R;

import java.util.ArrayList;

public class CourseDetailListViewAdapter  extends BaseAdapter {
//    private ArrayList<ListVO> listVO = new ArrayList<ListVO>() ;  videolist
        private ArrayList<VideoListVO> videolist = new ArrayList<VideoListVO>() ;
    public CourseDetailListViewAdapter() {

    }

    @Override
    public int getCount() {
        return videolist.size() ;
    }
    @Override
    public long getItemId(int position) {
        return position ;
    }
    @Override
    public Object getItem(int position) {
        return videolist.get(position) ;
    }


    // ** 이 부분에서 리스트뷰에 데이터를 넣어줌 **
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        //postion = ListView의 위치      /   첫번째면 position = 0
        final int pos = position;
        final Context context = parent.getContext();


        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.activity_course_detail_custom_listview, parent, false);
        }
        //TextView title = (TextView) convertView.findViewById(R.id.lv_course_id) ;
        TextView Context = (TextView) convertView.findViewById(R.id.lv_course_name) ;
        TextView time = (TextView) convertView.findViewById(R.id.lv_course_time) ;


        VideoListVO listViewItem = videolist.get(position);

        // 아이템 내 각 위젯에 데이터 반영
       // title.setText("Chapter");
        Context.setText(listViewItem.getTitle());
        time.setText(listViewItem.getTotalTime());

        //리스트뷰 클릭 이벤트
        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, VideoActivity.class);
                context.startActivity(intent);
            }
        });
        return convertView;
    }

    public void addVideoItem(String v_id, String title,  String total_time) {
        VideoListVO videoData = new VideoListVO();

        videoData.setTitle(title);
        videoData.setTotalTime(total_time);
        videolist.add(videoData);
    }

/*    // 데이터값 넣어줌
    public void addVO(String title, String desc) {
        ListVO item = new ListVO();
        item.setTitle(title);
        item.setContext(desc);

        listVO.add(item);
    }*/
}

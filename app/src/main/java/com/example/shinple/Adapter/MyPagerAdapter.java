package com.example.shinple.Adapter;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.example.shinple.MyClass.FragmentOne;
import com.example.shinple.MyClass.FragmentThree;
import com.example.shinple.MyClass.FragmentTwo;

/* 이미지 슬라이더 Adapter 입니다. */
public class MyPagerAdapter extends FragmentStatePagerAdapter {


    private int tabCount;

    public MyPagerAdapter(FragmentManager fragmentManager, int tabCount) {
        super(fragmentManager);
        this.tabCount = tabCount;
    }

    public MyPagerAdapter(FragmentManager supportFragmentManager) {
        super(supportFragmentManager);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
        case 0: {
            FragmentOne fragmentOne = new FragmentOne();
            return fragmentOne;
        }
        case 1: {
            FragmentTwo fragmentTwo = new FragmentTwo();
            return fragmentTwo;
        }
        case 2: {
            FragmentThree fragmentThree = new FragmentThree();
            return fragmentThree;
        }

        default:
            return null;
    }
}

    @Override
    public int getCount() {
        return tabCount;
    }


}
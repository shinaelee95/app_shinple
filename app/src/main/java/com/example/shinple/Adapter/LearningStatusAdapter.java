package com.example.shinple.Adapter;

        import android.content.Context;
        import android.graphics.Color;
        import android.graphics.PorterDuff;
        import android.view.LayoutInflater;
        import android.view.View;
        import android.view.ViewGroup;
        import android.widget.BaseAdapter;
        import android.widget.ImageView;
        import android.widget.ProgressBar;
        import android.widget.TextView;

        import com.example.shinple.ListVO.LearnVO;
        import com.example.shinple.R;
        import com.squareup.picasso.Picasso;

        import org.w3c.dom.Text;

        import java.util.ArrayList;
/*강의실 게시판 Adapter입니다.*/

public class LearningStatusAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<LearnVO> learninglist = new ArrayList<LearnVO>();
    // List<Learning> learninglist;
    private int value;

    public LearningStatusAdapter() {        //Context context, List<Learning> learninglist
//        this.context = context;
//        this.learninglist = learninglist;
    }


    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @Override
    public int getCount() {
        return learninglist.size();
    }

    @Override
    public Object getItem(int i) {
        return learninglist.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        /*        if(view == null){
            view = View.inflate(context, R.layout.learning_status, null);

        }*/
        final int pos = position;
        final Context context = viewGroup.getContext();

        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.learning_status, viewGroup, false);
        }

        TextView learning_category =(TextView)view.findViewById(R.id.learning_category);
        TextView lecture_name = (TextView) view.findViewById(R.id.lecture_name);
        //TextView learning_date = (TextView) view.findViewById(R.id.learning_date);
        TextView date_num = (TextView) view.findViewById(R.id.date_num);
        //TextView progress_rate = (TextView) view.findViewById(R.id.progress_rate);
        ProgressBar progress_my = (ProgressBar) view.findViewById(R.id.progress_my);
        TextView percent = (TextView)view.findViewById(R.id.progress_percent);
        ImageView image_path = (ImageView)view.findViewById(R.id.main_bottom_image_view);

        LearnVO learningListItem = learninglist.get(pos);

        learning_category.setText("[" + learningListItem.getLecture_subcategory() +"]" );                                    // 강좌 서브 카테고리 명
        lecture_name.setText(learningListItem.getLecture_name());                                                 // 강좌 타이틀

        Picasso.with(image_path.getContext())
                .load(learningListItem.getThumbnail())
                .into(image_path);
        date_num.setText(learningListItem.getStart_date());        // 강좌 수강 기간 ( start_date ~ end_ date)

        int percentData = learningListItem.getProgress_rate();
        if(percentData == 100){
            progress_my.getProgressDrawable().setColorFilter(Color.GREEN, PorterDuff.Mode.SRC_IN);
        }
        else{
            progress_my.getProgressDrawable().setColorFilter(Color.BLUE, PorterDuff.Mode.SRC_IN);
        }
        progress_my.setProgress(percentData);                                                 // 강좌 진도율
        percent.setText(percentData+"%");
        // -> 이건 어떤 기능?
        //view.setTag(learningListItem.getLecture_subcategory());

        return view;
    }

/*            view.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Intent intent = new Intent(context, CourseDetailActivity.class);
            context.startActivity(intent);
        }
    });*/


    //190828 민정 - 추가
    public void addItem(int courseId, String subCategory,String thumbnail, String title,  String startDate, String endDate,String completedCnt ,String totalCnt, String status) {
        LearnVO item = new LearnVO();

        item.setCourse_id(courseId);
        item.setLecture_subcategory(subCategory);
        item.setLecture_name(title);
        item.setStart_date(startDate);
        item.setEnd_date(endDate);

        // 강좌 진도율 계산
        int comp_cnt = Integer.parseInt(completedCnt);
        int total_cnt = Integer.parseInt(totalCnt);
        int percent = 0;
        if(comp_cnt > 0)
        {
             percent = (int)(((float)comp_cnt / (float)total_cnt) * 100);
        }

        item.setCompleted_Cnt(Integer.parseInt(completedCnt));
        item.setTotal_Cnt(Integer.parseInt(totalCnt));
        item.setProgress_rate(percent);

        item.setLecture_status(Integer.parseInt(status));
        item.setThumbnail(thumbnail);
        learninglist.add(item);
}
}


//190828 이영 -  마이페이지 - 수강한 강좌 리스트 목록 -  버튼 이벤트 주석처리
/*
        final Button btn = (Button) v.findViewById(R.id.room_button);
//        btn.setOnClickListener(
//                new Button.OnClickListener() {
//                    public void onClick(View v) {
//                        Intent intent = new Intent(getActivity(),qna_write.class);
//                        getActivity().startActivity(intent);
//                    }
//                }
//        );
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, CourseDetailActivity.class);
                context.startActivity(intent);
            }
        });

 */

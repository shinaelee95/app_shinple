package com.example.shinple.Note;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.DragEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.shinple.App_DB.RestClient;
import com.example.shinple.Course.CourseActivity;
import com.example.shinple.Functions.Note_SslFunc;
import com.example.shinple.MainMenu2Activity;
import com.example.shinple.MyClass.VideoListActivity;
import com.example.shinple.Notice.NoticeActivity;
import com.example.shinple.Notice.NoticeDetail;
import com.example.shinple.R;
import com.nightonke.boommenu.BoomButtons.OnBMClickListener;
import com.nightonke.boommenu.BoomButtons.TextOutsideCircleButton;
import com.nightonke.boommenu.BoomMenuButton;

public class NoteDetail extends Activity implements View.OnDragListener  {
    TextView[] textResponse = new TextView[4];  //원하는 TextView 크기 지정
    String text_column;
    RestClient restClient; //RestClient class를 이용해야함 필요 시 만들어서 사용
    int pass_c_id, pass_v_id, pass_n_id;
    String pass_c_name, pass_v_title, pass_created_date;
    BoomMenuButton bmb;
    private final int START_DRAG = 0;
    private final int END_DRAG = 1;
    private int isMoving;
    private float offset_x, offset_y;
    private boolean start_yn = true;

    // c_id, v_id는 note와 video에 있음
    //

    protected void onCreate(Bundle savedInstanceState) { //화면 만들기 active_notice.xml 화면띄움
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_note_detail); // notice 공지사항 화면 띄우기
        Intent get_intent = getIntent();

//        pass_c_id= Integer.parseInt(get_intent.getStringExtra("c_id")); // c_id 받아오기
//        pass_v_id= Integer.parseInt(get_intent.getStringExtra("v_id")); // v_id 받아오기
        pass_n_id= Integer.parseInt(get_intent.getStringExtra("n_id")); //n_id 받아오기

        pass_c_name=get_intent.getStringExtra("c_name");
        pass_v_title=get_intent.getStringExtra("v_title");
        pass_created_date=get_intent.getStringExtra("created_date");

//        System.out.println("c_name = " + pass_c_name);
//        System.out.println("v_title= " + pass_v_title);
//        System.out.println("created_date = " + pass_created_date);
//        System.out.println("c_id = " + pass_c_id );
        System.out.println("n_id = " + pass_n_id );

        init();
        settingBoommenu();
        bmb.setDraggable(true);
        bmb.setOnDragListener(this);


    }

   private void init() {
        getViews();
        restClient = RestClient.getInstance();
        restClient.parseDB("https://shinple.kr/app_db/note_tbl.php");  // 불러오고 싶은 DB 테이블 설정!!
        setListeners();
    }

    private void getViews() {
        textResponse[0] = (TextView) findViewById(R.id.note_note);

        textResponse[1] = (TextView) findViewById(R.id.note_c_name);
        textResponse[1].setText(pass_c_name);

        textResponse[2] = (TextView) findViewById(R.id.note_v_title);
        textResponse[2].setText(pass_v_title);

        textResponse[3] = (TextView) findViewById(R.id.note_created_date);
        textResponse[3].setText(pass_created_date);

        text_column = "note";
    }

    private void setListeners() {                                   // request에 따른 response 함수
        new Thread(new Runnable() {
            @Override
            public void run() {
                String response = restClient.postRequest(); // response 내용 저장
                System.out.println(response);
                setText(response, textResponse); //파씽 함수 고고
            }
        }).start();
    }

    private void setText(final String response, final TextView[] textRes) {
        NoteDetail.this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Note_SslFunc.ParseJSON(response, textRes, text_column, pass_n_id);
            }
        });
    }

    // SSL 통신 시 필요  -------------------------------------------------------------------------------------------------------------------------------------

// NoticeDetail 오류가 나서 주석처리했습니당
//    public void onClick_find(View v) { //test 검색을 누르면 화면전환(이 부분은 검색 목록이 떠야함)
//        Intent findIntent = new Intent(getApplicationContext(), FindActivity.class);
//        startActivity(findIntent);  //find activity 실행
//        finish();
//    }

    public void onClick_notice_str(View v) { //공지사항을 누르면 공지사항 home으로 이동!!
        Intent notice_strIntent = new Intent(getApplicationContext(), NoteActivity.class);
        startActivity(notice_strIntent); //notice activity 실행
        finish();
    }

    public void settingBoommenu() {
        bmb = (BoomMenuButton) findViewById(R.id.bmb1);
        bmb.setNormalColor(getResources().getColor(R.color.colorPrimaryP));
        //마이 페이지 버튼 구현
        TextOutsideCircleButton.Builder home_builder = new TextOutsideCircleButton.Builder().normalImageRes(R.drawable.mypage1);
        home_builder.normalText("메인메뉴");
        home_builder.normalColorRes(R.color.white);
        home_builder.normalTextColorRes(R.color.colorNull);
        home_builder.listener(new OnBMClickListener() {
            @Override
            public void onBoomButtonClick(int index) {

                Intent home_intent = new Intent(NoteDetail.this,MainMenu2Activity.class);
                startActivity(home_intent);

            }
        });
        bmb.addBuilder(home_builder);

        //전체 강좌 버튼 구현
        TextOutsideCircleButton.Builder lecture_builder = new TextOutsideCircleButton.Builder().normalImageRes(R.drawable.lists);
        lecture_builder.normalText("전체강좌");
        lecture_builder.normalColorRes(R.color.white);
        lecture_builder.normalTextColorRes(R.color.colorNull);
        lecture_builder.listener(new OnBMClickListener() {
            @Override
            public void onBoomButtonClick(int index) {
                Intent course_intent = new Intent(NoteDetail.this, CourseActivity.class);
                startActivity(course_intent);
            }
        });
        bmb.addBuilder(lecture_builder);
        //공지 사항 버튼 구현
        TextOutsideCircleButton.Builder notice_builder = new TextOutsideCircleButton.Builder().normalImageRes(R.drawable.notice);
        notice_builder.normalText("공지사항");
        notice_builder.normalColorRes(R.color.white);
        notice_builder.normalTextColorRes(R.color.colorNull);
        notice_builder.listener(new OnBMClickListener() {
            @Override
            public void onBoomButtonClick(int index) {
                Intent notice_intent = new Intent(NoteDetail.this, NoticeActivity.class);
                startActivity(notice_intent);
            }
        });
        bmb.addBuilder(notice_builder);
        //강의노트 게시판 버튼 구현
        TextOutsideCircleButton.Builder note_builder = new TextOutsideCircleButton.Builder().normalImageRes(R.drawable.note);
        note_builder.normalText("강의노트");
        note_builder.normalColorRes(R.color.white);
        note_builder.normalTextColorRes(R.color.colorNull);
        note_builder.listener(new OnBMClickListener() {
            @Override
            public void onBoomButtonClick(int index) {
                Intent note_intent = new Intent(NoteDetail.this, NoteActivity.class);
                startActivity(note_intent);
            }
        });
        bmb.addBuilder(note_builder);


    }

    @Override
    public boolean onDrag(View v, DragEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            if (start_yn) {
                offset_x = event.getX();
                offset_y = event.getY();
                start_yn = false;
            }
            isMoving = START_DRAG;
        } else if (event.getAction() == MotionEvent.ACTION_UP) {
            isMoving = END_DRAG;
        } else if (event.getAction() == MotionEvent.ACTION_MOVE) {
            if (isMoving == START_DRAG) {
                v.setX((int) event.getX() - offset_x);
                v.setY((int) event.getY() - offset_y);
            }
        }
        return false;
    }



}


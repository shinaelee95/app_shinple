package com.example.shinple.Note;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.DragEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.shinple.Adapter.NoteListViewAdapter;
import com.example.shinple.App_DB.RestClient;
import com.example.shinple.Course.CourseActivity;
import com.example.shinple.ListVO.NoteListVO;
import com.example.shinple.MainActivity;
import com.example.shinple.MainMenu2Activity;
import com.example.shinple.MyClass.VideoListActivity;
import com.example.shinple.Notice.NoticeActivity;
import com.example.shinple.R;
import com.nightonke.boommenu.BoomButtons.OnBMClickListener;
import com.nightonke.boommenu.BoomButtons.TextOutsideCircleButton;
import com.nightonke.boommenu.BoomMenuButton;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class NoteActivity extends AppCompatActivity implements View.OnClickListener,View.OnDragListener {
    ListView listview;
    NoteListViewAdapter note_adapter;
    BoomMenuButton bmb;
    private final int START_DRAG = 0;
    private final int END_DRAG = 1;
    private int isMoving;
    private float offset_x, offset_y;
    private boolean start_yn = true;


    protected void onCreate(Bundle savedInstanceState) { //화면 만들기 active_note.xml 화면띄움
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_note); // note 공지사항 화면 띄우기
        init();
        settingBoommenu();
        bmb.setDraggable(true);   // 붐 메뉴 버튼 드래그 가능하도록 설정
        bmb.setOnDragListener(this);

    }

    // SSL 통신 시 필요 : DB 추가 부분 ------------------------------------------------------------------------------------------------------------------------
    TextView[] textResponse = new TextView[3];
    String[] text_column = new String[4];
    RestClient restClient; //RestClient class를 이용해야함 필요 시 만들어서 사용


    private void init() {
        text_column[0] = "c_name";
        text_column[1] = "v_title";
        text_column[2] = "created_date";
//        text_column[3] = "c_id";
//        text_column[4] = "v_id";
        text_column[3] = "n_id";

        getViews();
        restClient = RestClient.getInstance();
        restClient.parseDB("https://shinple.kr/app_db/note_video_tbl.php");  // 불러오고 싶은 DB 테이블 설정!!
        setListeners();
    }


    private void getViews() {
        textResponse[0] = (TextView) findViewById(R.id.c_name_text);
        textResponse[1] = (TextView) findViewById(R.id.v_title_text);
        textResponse[2] = (TextView) findViewById(R.id.created_date_text);
    }

    private void setListeners() {                                   // request에 따른 response 함수
        new Thread(new Runnable() {
            @Override
            public void run() {
                String response = restClient.postRequest(); // response 내용 저장
                setText(response, textResponse); //파씽 함수 고고
            }
        }).start();
    }

    private void setText(final String response, final TextView[] textRes) {
        NoteActivity.this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                listview = (ListView) findViewById(R.id.noteListView);
                ArrayList<NoteListVO> noteList = new ArrayList<NoteListVO>();
                note_adapter = new NoteListViewAdapter();

                try {

                    JSONObject json = new JSONObject(response);
                    JSONArray arr = json.getJSONArray("result");
                    //임의의 리스트를 지정하여 값을 넣어주는 틀을 만듭니다.
                    final List<String> c_name = new ArrayList<String>();
                    final List<String> v_title = new ArrayList<String>();
                    final List<String> created_date = new ArrayList<String>();
//                    final List<Integer> c_id = new ArrayList<Integer>();
//                    final List<Integer> v_id = new ArrayList<Integer>();
                    final List<Integer> n_id = new ArrayList<Integer>();
                    JSONObject json2;

                    for (int i = 0; i < arr.length(); i++) {
                        json2 = arr.getJSONObject(i);
                        String note_c_name = json2.getString(text_column[0]);
                        String note_v_title = json2.getString(text_column[1]);
                        String note_created_date = json2.getString(text_column[2]);
//                        int note_c_id = json2.getInt(text_column[3]);
//                        int note_v_id = json2.getInt(text_column[4]);
                        int note_n_id = json2.getInt(text_column[3]);
                        c_name.add(note_c_name);
                        v_title.add(note_v_title);
                        created_date.add(note_created_date);
//                        c_id.add(note_c_id);
//                        v_id.add(note_v_id);
                        n_id.add(note_n_id);
                    }

                    for (int i = 0; i < arr.length(); i++) {
//                        note_adapter.addVO(c_name.get(i), v_title.get(i), created_date.get(i), c_id.get(i), v_id.get(i), n_id.get(i)); //갱신도 이루어짐
                        note_adapter.addVO(c_name.get(i), v_title.get(i), created_date.get(i), n_id.get(i)); //갱신도 이루어짐
                    }
                    listview.setAdapter(note_adapter); //adapter에 추가
                    listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView parent, View view, int position, long id ) {
                            Intent detailIntent = new Intent(getApplicationContext(), NoteDetail.class);
//                            detailIntent.putExtra("c_id", Integer.toString(c_id.get(position)));
//                            detailIntent.putExtra("v_id", Integer.toString(v_id.get(position)));
                            detailIntent.putExtra("c_name",c_name.get(position));
                            detailIntent.putExtra("v_title",v_title.get(position));
                            detailIntent.putExtra("created_date",created_date.get(position));
                            detailIntent.putExtra("n_id",Integer.toString(n_id.get(position)));
                            startActivity(detailIntent);
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }
    // SSL 통신 시 필요  -------------------------------------------------------------------------------------------------------------------------------------


//    public void onClick_find(View v) { //test 검색을 누르면 화면전환(이 부분은 검색 목록이 떠야함)
//        Intent findIntent = new Intent(getApplicationContext(), FindActivity.class);
//        startActivity(findIntent);  //find activity 실행
//        finish();
//    }

    public void onClick_note_str(View v) { //공지사항을 누르면 공지사항 home으로 이동!!
        Intent note_strIntent = new Intent(getApplicationContext(), NoteActivity.class);
        startActivity(note_strIntent); //note activity 실행
        finish();
    }

    public void onClick(View v){

    }
    public void settingBoommenu() {
        bmb = (BoomMenuButton) findViewById(R.id.bmb1);
        bmb.setNormalColor(getResources().getColor(R.color.colorPrimaryP));
        //마이 페이지 버튼 구현
        TextOutsideCircleButton.Builder home_builder = new TextOutsideCircleButton.Builder().normalImageRes(R.drawable.mypage1);
        home_builder.normalText("메인메뉴");
        home_builder.normalColorRes(R.color.white);
        home_builder.normalTextColorRes(R.color.colorNull);
        home_builder.listener(new OnBMClickListener() {
            @Override
            public void onBoomButtonClick(int index) {

                Intent home_intent = new Intent(NoteActivity.this,MainMenu2Activity.class);
                startActivity(home_intent);

            }
        });
        bmb.addBuilder(home_builder);

        //전체 강좌 버튼 구현
        TextOutsideCircleButton.Builder lecture_builder = new TextOutsideCircleButton.Builder().normalImageRes(R.drawable.lists);
        lecture_builder.normalText("전체강좌");
        lecture_builder.normalColorRes(R.color.white);
        lecture_builder.normalTextColorRes(R.color.colorNull);
        lecture_builder.listener(new OnBMClickListener() {
            @Override
            public void onBoomButtonClick(int index) {
                Intent course_intent = new Intent(NoteActivity.this, CourseActivity.class);
                startActivity(course_intent);
            }
        });
        bmb.addBuilder(lecture_builder);
        //공지 사항 버튼 구현
        TextOutsideCircleButton.Builder notice_builder = new TextOutsideCircleButton.Builder().normalImageRes(R.drawable.notice);
        notice_builder.normalText("공지사항");
        notice_builder.normalColorRes(R.color.white);
        notice_builder.normalTextColorRes(R.color.colorNull);
        notice_builder.listener(new OnBMClickListener() {
            @Override
            public void onBoomButtonClick(int index) {
                Intent notice_intent = new Intent(NoteActivity.this, NoticeActivity.class);
                startActivity(notice_intent);
            }
        });
        bmb.addBuilder(notice_builder);
        //강의노트 게시판 버튼 구현
        TextOutsideCircleButton.Builder note_builder = new TextOutsideCircleButton.Builder().normalImageRes(R.drawable.note);
        note_builder.normalText("강의노트");
        note_builder.normalColorRes(R.color.white);
        note_builder.normalTextColorRes(R.color.colorNull);
        note_builder.listener(new OnBMClickListener() {
            @Override
            public void onBoomButtonClick(int index) {
//                Intent note_intent = new Intent(NoteActivity.this, NoteActivity.class);
//                startActivity(note_intent);
            }
        });
        bmb.addBuilder(note_builder);


    }
    @Override
    public boolean onDrag(View v, DragEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            if (start_yn) {
                offset_x = event.getX();
                offset_y = event.getY();
                start_yn = false;
            }
            isMoving = START_DRAG;
        } else if (event.getAction() == MotionEvent.ACTION_UP) {
            isMoving = END_DRAG;
        } else if (event.getAction() == MotionEvent.ACTION_MOVE) {
            if (isMoving == START_DRAG) {
                v.setX((int) event.getX() - offset_x);
                v.setY((int) event.getY() - offset_y);
            }
        }
        return false;
    }


}
package com.example.shinple.Notice;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.DragEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;
import com.example.shinple.App_DB.RestClient;
import com.example.shinple.Course.CourseActivity;
import com.example.shinple.ListVO.NoticeListVO;
import com.example.shinple.MainActivity;
import com.example.shinple.MainMenu2Activity;
import com.example.shinple.MainMenuActivity;
import com.example.shinple.Note.NoteActivity;
import com.example.shinple.R;
import org.json.JSONArray;
import org.json.JSONObject;
import android.widget.ListView;
import android.widget.Toast;

import com.example.shinple.Adapter.NoticeListViewAdapter;
import com.nightonke.boommenu.BoomButtons.OnBMClickListener;
import com.nightonke.boommenu.BoomButtons.TextOutsideCircleButton;
import com.nightonke.boommenu.BoomMenuButton;

import java.util.ArrayList;
import java.util.List;


public class NoticeActivity extends AppCompatActivity implements View.OnClickListener ,View.OnDragListener{
    ListView listview;
    NoticeListViewAdapter notice_adapter;
    BoomMenuButton bmb;
    private final int START_DRAG = 0;
    private final int END_DRAG = 1;
    private int isMoving;
    private float offset_x, offset_y;
    private boolean start_yn = true;

    protected void onCreate(Bundle savedInstanceState) { //화면 만들기 active_notice.xml 화면띄움
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notice); // notice 공지사항 화면 띄우기
        init();
        settingBoommenu();
        bmb.setDraggable(true);   // 붐 메뉴 버튼 드래그 가능하도록 설정
        bmb.setOnDragListener(this);
    }

    // SSL 통신 시 필요 : DB 추가 부분 ------------------------------------------------------------------------------------------------------------------------
    TextView[] textResponse = new TextView[4];
    String[] text_column = new String[5];
    RestClient restClient; //RestClient class를 이용해야함 필요 시 만들어서 사용


    private void init() {
        text_column[0] = "title";
        text_column[1] = "created_date";
        text_column[2] = "admin_id";
        text_column[3] = "view_count";
        text_column[4] = "n_id";

        getViews();
        restClient = RestClient.getInstance();
        restClient.parseDB("https://shinple.kr/app_db/notice_tbl.php");  // 불러오고 싶은 DB 테이블 설정!!
        setListeners();
    }


    private void getViews() {
        textResponse[0] = (TextView) findViewById(R.id.title_text);
        textResponse[1] = (TextView) findViewById(R.id.created_date_text);
        textResponse[2] = (TextView) findViewById(R.id.admin_id_text);
        textResponse[3] = (TextView) findViewById(R.id.view_count_text);
    }

    private void setListeners() {                                   // request에 따른 response 함수
        new Thread(new Runnable() {
            @Override
            public void run() {
                String response = restClient.postRequest(); // response 내용 저장
                setText(response, textResponse); //파씽 함수 고고
            }
        }).start();
    }

    private void setText(final String response, final TextView[] textRes) {
        NoticeActivity.this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                listview = (ListView) findViewById(R.id.noticeListView);
                //ArrayList<NoticeListVO> noitceList = new ArrayList<NoticeListVO>();
                notice_adapter = new NoticeListViewAdapter();
                try {

                    JSONObject json = new JSONObject(response);
                    JSONArray arr = json.getJSONArray("result");
                    //임의의 리스트를 지정하여 값을 넣어주는 틀을 만듭니다.
                    List<String> title = new ArrayList<String>();
                    List<String> created_date = new ArrayList<String>();
                    List<String> admin_id = new ArrayList<String>();
                    List<String> view_count = new ArrayList<String>();
                    final List<Integer> n_id = new ArrayList<Integer>();
//                    notice_adapter.set_u_id(n_id); // n_id 수집

                    JSONObject json2;

                    for (int i = 0; i < arr.length(); i++) {
                        json2 = arr.getJSONObject(i);
                        String notice_title = json2.getString(text_column[0]);
                        String notice_created_date = json2.getString(text_column[1]);
                        String notice_admin_id = json2.getString(text_column[2]);
                        String notice_view_count = json2.getString(text_column[3]);
                        int notice_n_id = json2.getInt(text_column[4]);
                        title.add(notice_title);
                        created_date.add(notice_created_date);
                        admin_id.add(notice_admin_id);
                        view_count.add(notice_view_count);
                        n_id.add(notice_n_id);
                    }

                    for (int i = 0; i < arr.length(); i++) {
                        notice_adapter.addVO(title.get(i), created_date.get(i), admin_id.get(i), view_count.get(i), n_id.get(i)); //갱신도 이루어짐
                    }

                    listview.setAdapter(notice_adapter); //adapter에 추가
                    listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView parent, View view, int position, long id ) {
                            Intent detailIntent = new Intent(getApplicationContext(), NoticeDetail.class);
                            //detailIntent.putExtra("u_id", "bb");
                            detailIntent.putExtra("n_id", Integer.toString(n_id.get(position)));
                            startActivity(detailIntent);
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    // SSL 통신 시 필요  -------------------------------------------------------------------------------------------------------------------------------------


//    public void onClick_find(View v) { //test 검색을 누르면 화면전환(이 부분은 검색 목록이 떠야함)
//        Intent findIntent = new Intent(getApplicationContext(), FindActivity.class);
//        startActivity(findIntent);  //find activity 실행
//        finish();
//    }

    public void onClick_notice_str(View v) { //공지사항을 누르면 공지사항 home으로 이동!!
        Intent notice_strIntent = new Intent(getApplicationContext(), NoticeActivity.class);
        startActivity(notice_strIntent); //notice activity 실행
        finish();
    }
    public void onClick(View v){

    }
    public void settingBoommenu() {
        bmb = (BoomMenuButton) findViewById(R.id.bmb1);
        bmb.setNormalColor(getResources().getColor(R.color.colorPrimaryP));
        //마이 페이지 버튼 구현
        TextOutsideCircleButton.Builder home_builder = new TextOutsideCircleButton.Builder().normalImageRes(R.drawable.mypage1);
        home_builder.normalText("메인메뉴");
        home_builder.normalColorRes(R.color.white);
        home_builder.normalTextColorRes(R.color.colorNull);
        home_builder.listener(new OnBMClickListener() {
            @Override
            public void onBoomButtonClick(int index) {
                Intent home_intent = new Intent(NoticeActivity.this, MainMenu2Activity.class);
                startActivity(home_intent);

            }
        });
        bmb.addBuilder(home_builder);

        //전체 강좌 버튼 구현
        TextOutsideCircleButton.Builder lecture_builder = new TextOutsideCircleButton.Builder().normalImageRes(R.drawable.lists);
        lecture_builder.normalText("전체강좌");
        lecture_builder.normalColorRes(R.color.white);
        lecture_builder.normalTextColorRes(R.color.colorNull);
        lecture_builder.listener(new OnBMClickListener() {
            @Override
            public void onBoomButtonClick(int index) {
                Intent course_intent = new Intent(NoticeActivity.this, CourseActivity.class);
                startActivity(course_intent);

            }
        });
        bmb.addBuilder(lecture_builder);
        //공지 사항 버튼 구현
        TextOutsideCircleButton.Builder notice_builder = new TextOutsideCircleButton.Builder().normalImageRes(R.drawable.notice);
        notice_builder.normalText("공지사항");
        notice_builder.normalColorRes(R.color.white);
        notice_builder.normalTextColorRes(R.color.colorNull);
        notice_builder.listener(new OnBMClickListener() {
            @Override
            public void onBoomButtonClick(int index) {
//                Intent notice_intent = new Intent(NoticeActivity.this, NoticeActivity.class);
//                startActivity(notice_intent);
            }
        });
        bmb.addBuilder(notice_builder);
        //강의노트 게시판 버튼 구현
        TextOutsideCircleButton.Builder note_builder = new TextOutsideCircleButton.Builder().normalImageRes(R.drawable.note);
        note_builder.normalText("강의노트");
        note_builder.normalColorRes(R.color.white);
        note_builder.normalTextColorRes(R.color.colorNull);
        note_builder.listener(new OnBMClickListener() {
            @Override
            public void onBoomButtonClick(int index) {
                Intent note_intent = new Intent(NoticeActivity.this, NoteActivity.class);
                startActivity(note_intent);
            }
        });
        bmb.addBuilder(note_builder);


    }

    @Override
    public boolean onDrag(View v, DragEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            if (start_yn) {
                offset_x = event.getX();
                offset_y = event.getY();
                start_yn = false;
            }
            isMoving = START_DRAG;
        } else if (event.getAction() == MotionEvent.ACTION_UP) {
            isMoving = END_DRAG;
        } else if (event.getAction() == MotionEvent.ACTION_MOVE) {
            if (isMoving == START_DRAG) {
                v.setX((int) event.getX() - offset_x);
                v.setY((int) event.getY() - offset_y);
            }
        }
        return false;
    }


}
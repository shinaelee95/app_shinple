package com.example.shinple.MyClass;


import android.graphics.Color;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import com.example.shinple.R;
import com.google.android.material.tabs.TabLayout;


public class MyClassRoom extends AppCompatActivity {
    ViewPager viewPager;
    TabLayout tabLayout;

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_my_class_room);

            viewPager = findViewById(R.id.view_pager);
            tabLayout = findViewById(R.id.tab_layout);

        tabLayout.addTab(tabLayout.newTab().setText("학습현황"));
        tabLayout.addTab(tabLayout.newTab().setText("나의 질문"));
        tabLayout.addTab(tabLayout.newTab().setText("나의 정보"));

        tabLayout.setSelectedTabIndicatorColor(Color.parseColor("#ffffff"));

        com.example.shinple.Adapter.MyPagerAdapter pagerAdapter = new com.example.shinple.Adapter.MyPagerAdapter(getSupportFragmentManager(), 3);
        viewPager.setAdapter(pagerAdapter);

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));

    }

}
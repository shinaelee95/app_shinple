package com.example.shinple.MyClass;

import android.content.Intent;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.shinple.Adapter.Qna;
import com.example.shinple.App_DB.QnaURLConnector;
import com.example.shinple.App_DB.VideoUrlConnection;
import com.example.shinple.MainFragmentThree;
import com.example.shinple.QnaTab;
import com.example.shinple.R;

public class qna_write extends AppCompatActivity {

    public EditText et1;
    public EditText et2;
    QnaURLConnector url;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.qna_write);

        url = new QnaURLConnector();

        et1 = findViewById(R.id.title);
        et2 = findViewById(R.id.write);

        //radio 선택옵션
        final RadioGroup rg = findViewById(R.id.radioGroup);

//        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(RadioGroup radioGroup, int i) {
//                switch (i) {
//                    case R.id.radio1:
//                        break;
//                    case R.id.radio2:
//                        break;
//                    case R.id.radio3:
//                        break;
//                }
//            }
//        });

        final Button btn_ok = (Button) findViewById(R.id.button1);
        btn_ok.setOnClickListener(new Button.OnClickListener(){
            public void onClick(View v){

                int id= rg.getCheckedRadioButtonId();

                RadioButton rb=(RadioButton) findViewById(id);
                System.out.println(rg.indexOfChild(rb));

//                System.out.println("id값:"+rb.getText().toString());

                String s1 = et1.getText().toString();
                String s2 = et2.getText().toString();
                System.out.println("s1: "+s1+" s2: "+s2);
                url.setData(rg.indexOfChild(rb),s1,s2,1);
                url.start();
                try {
                    url.join();
                }
                catch(Exception e){
                    e.printStackTrace();
                }

                //Intent intent = new Intent(getApplicationContext(), MyClassRoom.class);
                //startActivity(intent);
                finish();
            }
        });


        String result = url.getTemp();
        System.out.println("URL result : "  + result);


        //버튼 클릭시 액티비티 전환
//        final Button btn = (Button) findViewById(R.id.button1);
//        btn.setOnClickListener(
//                new Button.OnClickListener() {
//                    public void onClick(View v) {
//                        Intent intent = new Intent(getApplicationContext(), Qna.class);
//                        startActivity(intent);
//                    }
//                }
//        );
    }
}



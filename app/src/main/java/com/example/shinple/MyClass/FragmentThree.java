package com.example.shinple.MyClass;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.shinple.App_DB.MyinfoURLConnector;
import com.example.shinple.R;

import org.json.JSONArray;
import org.json.JSONObject;


public class FragmentThree extends Fragment {
    TextView textView;
    TextView textView2;
    TextView textView3;
    TextView textView4;
    TextView textView5;
    TextView textView6;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_three, container, false);

        textView = (TextView) view.findViewById(R.id.name);
        textView2 = (TextView) view.findViewById(R.id.group);
        textView3 = (TextView) view.findViewById(R.id.team);
        textView4 = (TextView) view.findViewById(R.id.comp_num);
        textView5 = (TextView) view.findViewById(R.id.email);
        textView6 = (TextView) view.findViewById(R.id.birthday);

        MyinfoURLConnector url = new MyinfoURLConnector();
        url.start();
        try {
            url.join();
        } catch (Exception e) {
            e.printStackTrace();
        }

        String result = url.getTemp();
        System.out.println("result : " + result);
        System.out.println(ParseJSON(result));

        ParseJSON(result);

        return view;
    }
    public String ParseJSON(String target) {

        try {
            JSONObject json = new JSONObject(target);

            JSONArray arr = json.getJSONArray("result");

//            for(int i = 0; i < arr.length(); i++){
            JSONObject json2 = arr.getJSONObject(0);

            System.out.println(json2.getString("name"));
            textView.setText(json2.getString("name"));
            textView2.setText(json2.getString("company_id"));
            textView3.setText(json2.getString("team"));
            textView4.setText(json2.getString("cp_id"));
            textView5.setText(json2.getString("email"));
            textView6.setText(json2.getString("birthday"));

//            }

            return "";
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }
}
package com.example.shinple.MyClass;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.shinple.Adapter.LearningStatusAdapter;
import com.example.shinple.App_DB.TakesCourseUrlConnection;
import com.example.shinple.Functions.TakesCourseFunc;
import com.example.shinple.ListVO.LearnVO;
import com.example.shinple.R;

import java.util.ArrayList;
import java.util.Map;

public class Complete extends Fragment {
    private ListView learningListView;
    TakesCourseUrlConnection url;
    public ArrayList<Map<String, String>> learningList;
    LearningStatusAdapter adapter = new LearningStatusAdapter();
    String u_id;
//    public LearningStatusAdapter ladapter;
//    public List<Learning> learningList;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        u_id = getActivity().getIntent().getStringExtra("u_id");
        View view = inflater.inflate(R.layout.tab_complete, container, false);
        learningListView = (ListView) view.findViewById(R.id.completeView);

        learningList = new ArrayList<>();


        // URL 연결 - 서버에서 json데이터 받아오기
        url = new TakesCourseUrlConnection();

        url.setCompleted(Integer.parseInt(u_id));
        url.start();
        try {
            url.join();
        }
        catch(Exception e){
            e.printStackTrace();
        }

        String result = url.getTemp();
        learningList = TakesCourseFunc.parseCourseJSON(result);

        adapter =  TakesCourseFunc.setLearingData(adapter, learningList);
        //ladapter  = new LearningStatusAdapter(this.getActivity().getApplicationContext(), learningList);
        learningListView.setAdapter(adapter);

        learningListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView parent, View view, int position, long id) {
                System.out.println("List Click");
                LearnVO item = (LearnVO) parent.getItemAtPosition(position);
                Intent intent = new Intent(getActivity(), VideoListActivity.class);
                intent.putExtra("subCategory", item.getLecture_subcategory());
                intent.putExtra("title", item.getLecture_name());
                intent.putExtra("courseId", item.getCourse_id());
                intent.putExtra("u_id", Integer.parseInt( u_id));

                startActivity(intent);
            }
        });

        return view;

    }

}


/*
        learningList.add(new Learning("[강의]", "IOS 개발", "수강일", "2019-08-22~2019-08-31", "진도율",
                87));
        learningList.add(new Learning("[강의]", "안드로이드 개발", "수강일", "2019-08-22~2019-08-31", "진도율",
                99));

        learningList.add(new Learning("[강의]", "안드로이드 개발", "수강일", "2019-08-22~2019-08-31", "진도율",
                99));

        learningList.add(new Learning("[강의]", "안드로이드 개발", "수강일", "2019-08-22~2019-08-31", "진도율",
                99));

        learningList.add(new Learning("[강의]", "안드로이드 개발", "수강일", "2019-08-22~2019-08-31", "진도율",
                99));

        learningList.add(new Learning("[강의]", "안드로이드 개발", "수강일", "2019-08-22~2019-08-31", "진도율",
                99));

        learningList.add(new Learning("[강의]", "안드로이드 개발", "수강일", "2019-08-22~2019-08-31", "진도율",
                99));

        learningList.add(new Learning("[강의]", "안드로이드 개발", "수강일", "2019-08-22~2019-08-31", "진도율",
                99));

        learningList.add(new Learning("[강의]", "안드로이드 개발", "수강일", "2019-08-22~2019-08-31", "진도율",
                99));
*/


package com.example.shinple.MyClass;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.shinple.R;

public class FragmentOne extends Fragment {
    FrameLayout whole, learned, complete, expired;
    View viewwh, viewlea, viewcom, viewex;
    TextView tvwhole, tvlearned, tvcomplete, tvexpired;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View fragment_one = inflater.inflate(R.layout.fragment_one, container, false);
        //INIT VIEWS
        init(fragment_one);
        //SET TABS ONCLICK
        whole.setOnClickListener(clik);
        learned.setOnClickListener(clik);
        complete.setOnClickListener(clik);
        expired.setOnClickListener(clik);
        //LOAD PAGE FOR FIRST
        loadPage(new Whole());
        tvwhole.setTextColor(getActivity().getResources().getColor(R.color.colorPrimary));
        return fragment_one;
    }

    public void init(View v) {
        whole = v.findViewById(R.id.whole);
        learned = v.findViewById(R.id.learn);
        complete = v.findViewById(R.id.complete);
        expired = v.findViewById(R.id.expired);
        viewwh = v.findViewById(R.id.viewwh);
        viewlea = v.findViewById(R.id.viewlea);
        viewcom = v.findViewById(R.id.viewcom);
        viewex = v.findViewById(R.id.viewex);
        tvwhole = v.findViewById(R.id.tvwhole);
        tvlearned = v.findViewById(R.id.tvlearn);
        tvcomplete = v.findViewById(R.id.tvcom);
        tvexpired = v.findViewById(R.id.tvex);
    }





    //ONCLICK LISTENER
    public View.OnClickListener clik = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.whole:
                    //ONSELLER CLICK
                    //LOAD Whole FRAGMENT CLASS
                    loadPage(new Whole());

                    //눌렀을 때 배경색깔?
                    tvwhole.setBackgroundColor(getActivity().getResources().getColor(R.color.colorNull));
                    tvlearned.setBackgroundColor(getActivity().getResources().getColor(R.color.colorNull));
                    tvcomplete.setBackgroundColor(getActivity().getResources().getColor(R.color.colorNull));
                    tvexpired.setBackgroundColor(getActivity().getResources().getColor(R.color.colorNull));
                    //WHEN CLICK TEXT COLOR CHANGED
                    tvwhole.setTextColor(getActivity().getResources().getColor(R.color.Primary));
                    tvlearned.setTextColor(getActivity().getResources().getColor(R.color.colorPrimaryDark));
                    tvcomplete.setTextColor(getActivity().getResources().getColor(R.color.colorPrimaryDark));
                    tvexpired.setTextColor(getActivity().getResources().getColor(R.color.colorPrimaryDark));
                    //VIEW VISIBILITY WHEN CLICKED
                    viewwh.setVisibility(View.VISIBLE);
                    viewlea.setVisibility(View.INVISIBLE);
                    viewcom.setVisibility(View.INVISIBLE);
                    viewex.setVisibility(View.INVISIBLE);
                    break;
                case R.id.learn:
                    //ONBUYER CLICK
                    //LOAD Learn FRAGMENT CLASS
                    loadPage(new Learn());


                    //WHEN CLICK TEXT COLOR CHANGED
                    tvwhole.setTextColor(getActivity().getResources().getColor(R.color.colorPrimaryDark));
                    tvlearned.setTextColor(getActivity().getResources().getColor(R.color.Primary));
                    tvcomplete.setTextColor(getActivity().getResources().getColor(R.color.colorPrimaryDark));
                    tvexpired.setTextColor(getActivity().getResources().getColor(R.color.colorPrimaryDark));
                    //VIEW VISIBILITY WHEN CLICKED
                    viewwh.setVisibility(View.INVISIBLE);
                    viewlea.setVisibility(View.VISIBLE);
                    viewcom.setVisibility(View.INVISIBLE);
                    viewex.setVisibility(View.INVISIBLE);
                    break;

                case R.id.complete:
                    //ONBUYER CLICK
                    //LOAD Complete FRAGMENT CLASS
                    loadPage(new Complete());


                    //WHEN CLICK TEXT COLOR CHANGED
                    tvwhole.setTextColor(getActivity().getResources().getColor(R.color.colorPrimaryDark));
                    tvlearned.setTextColor(getActivity().getResources().getColor(R.color.colorPrimaryDark));
                    tvcomplete.setTextColor(getActivity().getResources().getColor(R.color.Primary));
                    tvexpired.setTextColor(getActivity().getResources().getColor(R.color.colorPrimaryDark));
                    //VIEW VISIBILITY WHEN CLICKED
                    viewwh.setVisibility(View.INVISIBLE);
                    viewlea.setVisibility(View.INVISIBLE);
                    viewcom.setVisibility(View.VISIBLE);
                    viewex.setVisibility(View.INVISIBLE);
                    break;

                case R.id.expired:
                    //ONBUYER CLICK
                    //LOAD Expired FRAGMENT CLASS
                    loadPage(new Expired());

                    //WHEN CLICK TEXT COLOR CHANGED
                    tvwhole.setTextColor(getActivity().getResources().getColor(R.color.colorPrimaryDark));
                    tvlearned.setTextColor(getActivity().getResources().getColor(R.color.colorPrimaryDark));
                    tvcomplete.setTextColor(getActivity().getResources().getColor(R.color.colorPrimaryDark));
                    tvexpired.setTextColor(getActivity().getResources().getColor(R.color.Primary));
                    //VIEW VISIBILITY WHEN CLICKED
                    viewwh.setVisibility(View.INVISIBLE);
                    viewlea.setVisibility(View.INVISIBLE);
                    viewcom.setVisibility(View.INVISIBLE);
                    viewex.setVisibility(View.VISIBLE);
                    break;
            }

        }

    };

    //LOAD PAGE FRAGMENT METHOD
    private boolean loadPage(Fragment fragment) {
        if (fragment != null) {
            getActivity().getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.containerpage, fragment)
                    .commit();
            return true;
        }
        return false;
    }
}
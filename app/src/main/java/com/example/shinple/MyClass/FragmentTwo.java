package com.example.shinple.MyClass;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.Fragment;

import com.example.shinple.Adapter.Qna;
import com.example.shinple.Adapter.QnaListAdapter;
import com.example.shinple.R;

import java.util.ArrayList;
import java.util.List;

public class FragmentTwo extends Fragment {
    private ListView qna_listView;
    private QnaListAdapter madapter;
    private List<Qna> qnaList;
    ImageView enterqna;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_two, container, false);


        final Button btn = (Button) view.findViewById(R.id.qna_write);
//        btn.setOnClickListener(
//                new Button.OnClickListener() {
//                    public void onClick(View v) {
//                        Intent intent = new Intent(getActivity(),qna_write.class);
//                        getActivity().startActivity(intent);
//                    }
//                }
//        );
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent1 = new Intent(getActivity(), qna_write.class);
                startActivity(intent1);
            }
        });

        List<String> categories = new ArrayList<String>();

        categories.add("전체");
        categories.add("강의");
        categories.add("기간");
        categories.add("고객");
        Spinner spinner = (Spinner) view.findViewById(R.id.spinner);
        spinner.setSelection(0);


        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this.getActivity(), R.layout.spinner_item, categories);



        spinner.setAdapter(adapter);


        qna_listView = (ListView) view.findViewById(R.id.qnaListView);
        qnaList = new ArrayList<Qna>();
        qnaList.add(new Qna("계정의 비밀번호를 잊어버렸습니다..", "[이용문의]", "2019-08-22"));
        qnaList.add(new Qna("나의 질문 내용", "[강의]", "2019-08-22"));
        qnaList.add(new Qna("나의 질문 내용", "[강의]", "2019-08-22"));
        qnaList.add(new Qna("나의 질문 내용", "[강의]", "2019-08-22"));
        qnaList.add(new Qna("나의 질문 내용", "[강의]", "2019-08-22"));
        qnaList.add(new Qna("나의 질문 내용", "[강의]", "2019-08-22"));

        madapter = new QnaListAdapter(this.getActivity().getApplicationContext(), qnaList);
        qna_listView.setAdapter(madapter);


//        Toolbar toolbar = (Toolbar) view.findViewById(R.id.search_tool);

//        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
//        toolbar.getTitle();

//        ((AppCompatActivity)getActivity()).setTitle("My Q\u0026A");


//        ((AppCompatActivity)getActivity()).setTitle("");

        setHasOptionsMenu(true);







                /*

        enterqna = (ImageView)view.findViewById(R.id.enterqna);
        enterqna.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(this, com.example.shinple.MyClassRoom.class);
                startActivity(intent);
            }
        });
        */








        //기본 Adapter로 view를 return 하기 때문에 Q&A 어댑터와 충돌이 날 수도 있습니다.
        //onViewCreated를 만들어주어서 따로 동작하게. Created 다음에 바로 보여주는 것이기 때문에 충돌방지할 수 있는 것 같습니다
        return view;
    }

    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_item, menu);
        SearchView searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {




            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                return false;
            }
        });

    }


}





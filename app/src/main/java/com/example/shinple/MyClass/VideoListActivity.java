package com.example.shinple.MyClass;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.DragEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.example.shinple.Adapter.VideoListAdapter;
import com.example.shinple.App_DB.VideoUrlConnection;
import com.example.shinple.Course.CourseActivity;
import com.example.shinple.Course.CourseContents;
import com.example.shinple.Course.VideoActivity;
import com.example.shinple.Course.VideoFragmentOne;
import com.example.shinple.ListVO.VideoListVO;
import com.example.shinple.MainMenu2Activity;
import com.example.shinple.Note.NoteActivity;
import com.example.shinple.Notice.NoticeActivity;
import com.example.shinple.R;
import com.nightonke.boommenu.BoomButtons.OnBMClickListener;
import com.nightonke.boommenu.BoomButtons.TextOutsideCircleButton;
import com.nightonke.boommenu.BoomMenuButton;

import org.json.JSONArray;
import org.json.JSONObject;

import java.nio.channels.InterruptedByTimeoutException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class VideoListActivity extends AppCompatActivity  implements View.OnDragListener{

    ProgressBar progressBar;
    int intValue = 0;
    Handler handler = new Handler();

    ArrayList<Map<String, String>> videoList = new ArrayList<>();
    Map<String, String > map;
    TextView courseTitle;
    String title;
    VideoUrlConnection url;
    int courseId, u_id;
    BoomMenuButton bmb;
    private final int START_DRAG = 0;
    private final int END_DRAG = 1;
    private int isMoving;
    private float offset_x, offset_y;
    private boolean start_yn = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_listview);

        System.out.println("비디오리스트뷰");
        Intent intent = getIntent(); /*데이터 수신*/

        String subCategory = intent.getExtras().getString("subCategory"); /*String형*/
        String title = intent.getExtras().getString("title"); /*String형*/
         courseId = intent.getExtras().getInt("courseId"); /*Int형*/
        u_id = intent.getExtras().getInt("u_id"); /*Int형*/

        String videoListTitle = "["+subCategory +"] " + title;

        ListView listview = (ListView) findViewById(R.id.videolistview);
        TextView titleTextView = (TextView) findViewById(R.id.videoTitle);
        VideoListAdapter adapter = new VideoListAdapter() ;

        titleTextView.setText(videoListTitle);

        // 리스트뷰 참조 및 Adapter달기
        listview.setAdapter(adapter);

        // URL 연결 - 서버에서 json데이터 받아오기
         url = new VideoUrlConnection();
         System.out.println("courseId : "  + courseId  + ", u_id : " + u_id);
        url.setData(courseId, u_id);

        url.start();
        try {
            url.join();
        }
        catch(Exception e){
            e.printStackTrace();
        }

        String result = url.getTemp();
        System.out.println("videolist URL result : "  + result);


        parseJSON(result);

        for (Map<String, String> map : videoList) {
            adapter.addItem( map.get("v_id"), map.get("title"),map.get("play_sec") , map.get("total_sec") ,  map.get("play_time") , map.get("total_time"), map.get("end_date") , map.get("video_path") ) ;
        }

        listview.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView parent, View v, int position, long id) {
                // get item
                VideoListVO item = (VideoListVO) parent.getItemAtPosition(position) ;
                //Drawable iconDrawable = item.getThumbnail() ;
                int videoId =item.getVideoId();
                String titleStr = item.getTitle() ;
                String videoPathStr = item.getVideoPath();
                String playTimeToSec= item.getPlayTimeToSec();
                String totlaTimeToSec =item.getTotalTimeToSec();
                String playTime = item.getPlayTime() ;
                String totalTime = item.getTotalTime();

                Intent intent = new Intent(getApplicationContext(), VideoActivity.class);


                System.out.println(" titleStr : " + titleStr + ", video Check : " + Integer.toString(videoId) + " . " +  videoPathStr + " . " + Integer.toString(courseId) );

                intent.putExtra("titleStr", titleStr);
                intent.putExtra("u_id", Integer.toString(u_id));
                intent.putExtra("v_id", Integer.toString(videoId));
                intent.putExtra("c_id", Integer.toString(courseId));
                intent.putExtra("video_path",videoPathStr);

//                VideoFragmentOne fragment = new VideoFragmentOne();
//                Bundle bundle = new Bundle();
//                bundle.putString("c_id",Integer.toString(courseId));
//                bundle.putString("v_id",Integer.toString(videoId));
//                bundle.putString("video_path",videoPathStr);
//                bundle.putString("u_id",Integer.toString(u_id));
//                fragment.setArguments(bundle);

                startActivity(intent);
            }
        }) ;

        titleTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               /* Intent intent = new Intent(view.getContext() , CourseContents.class);
                startActivity(intent);*/
            }
        });

        settingBoommenu();
        bmb.setDraggable(true);
        bmb.setOnDragListener(this);

    }

    public void settingBoommenu() {
        bmb = (BoomMenuButton) findViewById(R.id.bmb1);
        bmb.setNormalColor(getResources().getColor(R.color.colorPrimaryP));
        //마이 페이지 버튼 구현
        TextOutsideCircleButton.Builder home_builder = new TextOutsideCircleButton.Builder().normalImageRes(R.drawable.mypage1);
        home_builder.normalText("메인메뉴");
        home_builder.normalColorRes(R.color.white);
        home_builder.normalTextColorRes(R.color.colorNull);
        home_builder.listener(new OnBMClickListener() {
            @Override
            public void onBoomButtonClick(int index) {
                Intent home_intent = new Intent(VideoListActivity.this,MainMenu2Activity.class);
                startActivity(home_intent);

            }
        });
        bmb.addBuilder(home_builder);

        //전체 강좌 버튼 구현
        TextOutsideCircleButton.Builder lecture_builder = new TextOutsideCircleButton.Builder().normalImageRes(R.drawable.lists);
        lecture_builder.normalText("전체강좌");
        lecture_builder.normalColorRes(R.color.white);
        lecture_builder.normalTextColorRes(R.color.colorNull);
        lecture_builder.listener(new OnBMClickListener() {
            @Override
            public void onBoomButtonClick(int index) {
                Intent course_intent = new Intent(VideoListActivity.this, CourseActivity.class);
                startActivity(course_intent);

            }
        });
        bmb.addBuilder(lecture_builder);
        //공지 사항 버튼 구현
        TextOutsideCircleButton.Builder notice_builder = new TextOutsideCircleButton.Builder().normalImageRes(R.drawable.notice);
        notice_builder.normalText("공지사항");
        notice_builder.normalColorRes(R.color.white);
        notice_builder.normalTextColorRes(R.color.colorNull);
        notice_builder.listener(new OnBMClickListener() {
            @Override
            public void onBoomButtonClick(int index) {
                Intent notice_intent = new Intent(VideoListActivity.this, NoticeActivity.class);
                startActivity(notice_intent);
            }
        });
        bmb.addBuilder(notice_builder);
        //강의노트 게시판 버튼 구현
        TextOutsideCircleButton.Builder note_builder = new TextOutsideCircleButton.Builder().normalImageRes(R.drawable.note);
        note_builder.normalText("강의노트");
        note_builder.normalColorRes(R.color.white);
        note_builder.normalTextColorRes(R.color.colorNull);
        note_builder.listener(new OnBMClickListener() {
            @Override
            public void onBoomButtonClick(int index) {
                Intent note_intent = new Intent(VideoListActivity.this, NoteActivity.class);
                startActivity(note_intent);
            }
        });
        bmb.addBuilder(note_builder);


    }

    @Override
    public boolean onDrag(View v, DragEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            if (start_yn) {
                offset_x = event.getX();
                offset_y = event.getY();
                start_yn = false;
            }
            isMoving = START_DRAG;
        } else if (event.getAction() == MotionEvent.ACTION_UP) {
            isMoving = END_DRAG;
        } else if (event.getAction() == MotionEvent.ACTION_MOVE) {
            if (isMoving == START_DRAG) {
                v.setX((int) event.getX() - offset_x);
                v.setY((int) event.getY() - offset_y);
            }
        }
        return false;
    }

    

    // JSON 데이터를 파싱합니다.
    // URLConnector로부터 받은 String이 JSON 문자열이기 때문입니다.
    public  void parseJSON(String target){

        try {
            JSONObject json = new JSONObject(target);

            JSONArray arr = json.getJSONArray("result");

            if(arr != null)
            {
                for(int i = 0; i < arr.length(); i++){

                    map =  new HashMap<>();
                    JSONObject obj = arr.getJSONObject(i);

//                  System.out.println("obj.getString(\"play_sec\") : " + obj.getString("play_sec"));
//                  System.out.println("obj.getString(\"total_sec\") : " + obj.getString("total_sec"));

                    map.put("v_id", obj.getString("v_id"));
                    map.put("title", obj.getString("v_title"));
                    map.put("video_path", obj.getString("video_path"));
                    map.put("play_sec", obj.getString("play_sec"));
                    map.put("total_sec", obj.getString("total_sec"));
                    map.put("play_time", obj.getString("play_time"));
                    map.put("total_time", obj.getString("total_time"));
                    map.put("end_date", obj.getString("end_date"));
                    // map.put("video_iscompleted", obj.getString("video_iscompleted"));
                    videoList.add(map);
                }
            }
        }
        catch(Exception e){
            System.out.println("json exception");

            e.printStackTrace();
        }
    }
}

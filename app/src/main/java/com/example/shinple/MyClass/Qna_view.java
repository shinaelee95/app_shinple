package com.example.shinple.MyClass;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.DragEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.shinple.Adapter.QnaListAdapter;
import com.example.shinple.App_DB.MyinfoURLConnector;
import com.example.shinple.App_DB.QnaURLConnector;
import com.example.shinple.App_DB.QnaViewConnector;
import com.example.shinple.Course.CourseActivity;
import com.example.shinple.MainMenu2Activity;
import com.example.shinple.Note.NoteActivity;
import com.example.shinple.Notice.NoticeActivity;
import com.example.shinple.QnaTab;
import com.example.shinple.R;
import com.nightonke.boommenu.BoomButtons.OnBMClickListener;
import com.nightonke.boommenu.BoomButtons.TextOutsideCircleButton;
import com.nightonke.boommenu.BoomMenuButton;

import org.json.JSONArray;
import org.json.JSONObject;

public class Qna_view extends AppCompatActivity implements View.OnDragListener{
    TextView textView1;
    TextView textView2;
    String u_id;
    BoomMenuButton bmb;
    private final int START_DRAG = 0;
    private final int END_DRAG = 1;
    private int isMoving;
    private float offset_x, offset_y;
    private boolean start_yn = true;


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.qna_view);
        u_id = this.getIntent().getStringExtra("u_id");

        textView1 = (TextView) findViewById(R.id.qnaTitle);
        textView2 = (TextView) findViewById(R.id.qnaContents);

        System.out.println("Qna View 호출됨");

        QnaViewConnector url = new QnaViewConnector(u_id);
        url.start();
        try {
            url.join();
        } catch (Exception e) {
            e.printStackTrace();
        }

        String result = url.getTemp();
        System.out.println("result : " + result);
        System.out.println(ParseJSON(result));

        ParseJSON(result);

        settingBoommenu();
        bmb.setDraggable(true);   // 붐 메뉴 버튼 드래그 가능하도록 설정
        bmb.setOnDragListener(this);
    }

    public String ParseJSON(String target) {
        //Qna_view에서 목록 클릭시 detail view 제공

        Intent intent = getIntent();
        int index = intent.getIntExtra("detail",0);


        try {
            JSONObject json = new JSONObject(target);

            JSONArray arr = json.getJSONArray("result");

//            for(int i = 0; i < arr.length(); i++){
            JSONObject json2 = arr.getJSONObject(index);

            System.out.println(json2.getString("contents"));
            textView1.setText(json2.getString("contents"));
            textView2.setText(json2.getString("answer"));
//            }

            return "";
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    public void settingBoommenu() {
        bmb = (BoomMenuButton) findViewById(R.id.bmb1);
        bmb.setNormalColor(getResources().getColor(R.color.colorPrimaryP));
        //마이 페이지 버튼 구현
        TextOutsideCircleButton.Builder home_builder = new TextOutsideCircleButton.Builder().normalImageRes(R.drawable.mypage1);
        home_builder.normalText("메인메뉴");
        home_builder.normalColorRes(R.color.white);
        home_builder.normalTextColorRes(R.color.colorNull);
        home_builder.listener(new OnBMClickListener() {
            @Override
            public void onBoomButtonClick(int index) {
                Intent home_intent = new Intent(Qna_view.this,MainMenu2Activity.class);
                startActivity(home_intent);

            }
        });
        bmb.addBuilder(home_builder);

        //전체 강좌 버튼 구현
        TextOutsideCircleButton.Builder lecture_builder = new TextOutsideCircleButton.Builder().normalImageRes(R.drawable.lists);
        lecture_builder.normalText("전체강좌");
        lecture_builder.normalColorRes(R.color.white);
        lecture_builder.normalTextColorRes(R.color.colorNull);
        lecture_builder.listener(new OnBMClickListener() {
            @Override
            public void onBoomButtonClick(int index) {
                Intent course_intent = new Intent(Qna_view.this, CourseActivity.class);
                startActivity(course_intent);

            }
        });
        bmb.addBuilder(lecture_builder);
        //공지 사항 버튼 구현
        TextOutsideCircleButton.Builder notice_builder = new TextOutsideCircleButton.Builder().normalImageRes(R.drawable.notice);
        notice_builder.normalText("공지사항");
        notice_builder.normalColorRes(R.color.white);
        notice_builder.normalTextColorRes(R.color.colorNull);
        notice_builder.listener(new OnBMClickListener() {
            @Override
            public void onBoomButtonClick(int index) {
                Intent notice_intent = new Intent(Qna_view.this, NoticeActivity.class);
                startActivity(notice_intent);
            }
        });
        bmb.addBuilder(notice_builder);
        //강의노트 게시판 버튼 구현
        TextOutsideCircleButton.Builder note_builder = new TextOutsideCircleButton.Builder().normalImageRes(R.drawable.note);
        note_builder.normalText("강의노트");
        note_builder.normalColorRes(R.color.white);
        note_builder.normalTextColorRes(R.color.colorNull);
        note_builder.listener(new OnBMClickListener() {
            @Override
            public void onBoomButtonClick(int index) {
                Intent note_intent = new Intent(Qna_view.this, NoteActivity.class);
                startActivity(note_intent);
            }
        });
        bmb.addBuilder(note_builder);


    }

    @Override
    public boolean onDrag(View v, DragEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            if (start_yn) {
                offset_x = event.getX();
                offset_y = event.getY();
                start_yn = false;
            }
            isMoving = START_DRAG;
        } else if (event.getAction() == MotionEvent.ACTION_UP) {
            isMoving = END_DRAG;
        } else if (event.getAction() == MotionEvent.ACTION_MOVE) {
            if (isMoving == START_DRAG) {
                v.setX((int) event.getX() - offset_x);
                v.setY((int) event.getY() - offset_y);
            }
        }
        return false;
    }







}


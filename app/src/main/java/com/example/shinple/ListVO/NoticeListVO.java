package com.example.shinple.ListVO;

public class NoticeListVO {
    String title;
    String created_date;
    String admin_id;
    String view_count;
    int n_id;

    public NoticeListVO() {
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCreated_date() {
        return created_date;
    }

    public void setCreated_date(String created_date) {
        this.created_date = created_date;
    }

    public String getAdmin_id() {
        return admin_id;
    }

    public void setAdmin_id(String admin_id) {
        this.admin_id = admin_id;
    }

    public String getView_count() {
        return view_count;
    }

    public void setView_count(String view_count) {
        this.view_count = view_count;
    }

    public int getN_id(){ return n_id; }

    public void setN_id(int n_id){ this.n_id = n_id; }
}
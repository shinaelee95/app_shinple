package com.example.shinple.ListVO;

public class DetailCourseVO {

    public String title;                    //강좌 제목 ( 타이틀 명)
    public String Thumbnail;               //강좌 썸네일 경로
    public String courseInfo;              //강좌 상세 설명
    public int period;                      //강좌 수강 기간
    public String instructor;               //강좌 강사명
    public int takesCnt;                    //수강 횟수 ( view_count)
    public int courseId;                    //강좌 ID(PK)
    public String subCategory;              //강좌 서브 카테고리

    public int getPeriod() {
        return period;
    }

    public String getInstructor() {
        return instructor;
    }

    public void setInstructor(String instructor) {
        this.instructor = instructor;
    }

    public int getTakesCnt() {
        return takesCnt;
    }

    public void setTakesCnt(int takesCnt) {
        this.takesCnt = takesCnt;
    }

    public int getCourseId() {
        return courseId;
    }

    public void setCourseId(int courseId) {
        this.courseId = courseId;
    }

    public void setPeriod(int period) {
        this.period = period;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getThumbnail() {
        return Thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        Thumbnail = thumbnail;
    }

    public String getCourseInfo() {
        return courseInfo;
    }

    public void setCourseInfo(String courseInfo) {
        this.courseInfo = courseInfo;
    }


    public String getSubCategory() {
        return subCategory;
    }

    public void setSubCategory(String subCategory) {
        this.subCategory = subCategory;
    }
}


package com.example.shinple.ListVO;

public class VideoListVO {
    private int videoId;                //비디오ID( PK )
    private String title;               //강의 영상 제목
    private String totalTime;           //강의 영상 재생 총 시간
    private String playTime;            //강의 영상 재생 시간
    private  String totalTimeToSec;
    private  String playTimeToSec;

    public String getTotalTime() {
        return totalTime;
    }

    public String getTotalTimeToSec() {
        return totalTimeToSec;
    }

    public void setTotalTimeToSec(String totalTimeToSec) {
        this.totalTimeToSec = totalTimeToSec;
    }

    public String getPlayTimeToSec() {
        return playTimeToSec;
    }

    public void setPlayTimeToSec(String playTimeToSec) {
        this.playTimeToSec = playTimeToSec;
    }

    public void setTotalTime(String totalTime) {
        this.totalTime = totalTime;
    }

    public String getPlayTime() {
        return playTime;
    }

    public void setPlayTime(String playTime) {
        this.playTime = playTime;
    }

    private String endDate;             // 이용 종료일
    private String videoPath;           //강의 영상 경로 (서버)



    public int getVideoId() {
        return videoId;
    }

    public void setVideoId(int videoId) {
        this.videoId = videoId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }


    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getVideoPath() {
        return videoPath;
    }

    public void setVideoPath(String videoPath) {
        this.videoPath = videoPath;
    }

}

package com.example.shinple.ListVO;

public class NoteListVO {
    String c_name;
    String v_title;
    String created_date;
    int c_id;
    int v_id;
    int n_id;

    public NoteListVO() {
    }

    public String getC_name() {
        return c_name;
    }

    public void setC_name(String c_name) {
        this.c_name = c_name;
    }

    public String getV_title() {
        return v_title;
    }

    public void setV_title(String v_title) {
        this.v_title = v_title;
    }

    public String getCreated_date() {
        return created_date;
    }

    public void setCreated_date(String created_date) {
        this.created_date = created_date;
    }

    public int getC_id(){ return c_id; }

    public void setC_id(int c_id){ this.c_id = c_id; }

    public int getV_id(){ return v_id; }

    public void setV_id(int v_id){ this.v_id = v_id; }

    public int getN_id(){ return n_id; }

    public void setN_id(int n_id){ this.n_id = n_id; }
}

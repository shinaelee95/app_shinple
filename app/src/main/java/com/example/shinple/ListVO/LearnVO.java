package com.example.shinple.ListVO;

public class LearnVO {

    private int course_id;
    private String lecture_subcategory;       //강좌카테고리
    private String lecture_name;                //강좌명
    private String start_date;                  //강좌 수강 시작일
    private String end_date;                    //강좌 수강 종료일
    private String thumbnail;
    private int lecture_status;                //강좌 진행 상태
    private  int total_Cnt;
    private  int completed_Cnt;
    private int progress_rate;                //프로그레스바 진도율

/*    public Learning(String lecture_subcategory, String lecture_name, String start_date, String end_date, String date_num,int progress_rate) {
        this.lecture_subcategory = lecture_subcategory;
        this.lecture_name = lecture_name;
        this.start_date = start_date;
        this.end_date = end_date;
        this.progress_rate = progress_rate;

    }*/

    public int getCourse_id() {
        return course_id;
    }

    public void setCourse_id(int course_id) {
        this.course_id = course_id;
    }

    public String getLecture_subcategory() {
        return lecture_subcategory;
    }

    public void setLecture_subcategory(String lecture_subcategory) {
        this.lecture_subcategory = lecture_subcategory;
    }

    public String getLecture_name() {
        return lecture_name;
    }

    public void setLecture_name(String lecture_name) {
        this.lecture_name = lecture_name;
    }

    public String getStart_date() {
        return start_date;
    }

    public void setStart_date(String start_date) {
        this.start_date = start_date;
    }

    public String getEnd_date() {
        return end_date;
    }

    public void setEnd_date(String end_date) {
        this.end_date = end_date;
    }

    public int getLecture_status() {
        return lecture_status;
    }

    public void setLecture_status(int lecture_status) {
        this.lecture_status = lecture_status;
    }

    public int getProgress_rate() {
        return progress_rate;
    }

    public void setProgress_rate(int progress_rate) {
        this.progress_rate = progress_rate;
    }

    public int getTotal_Cnt() {
        return total_Cnt;
    }

    public void setTotal_Cnt(int total_Cnt) {
        this.total_Cnt = total_Cnt;
    }

    public int getCompleted_Cnt() {
        return completed_Cnt;
    }

    public void setCompleted_Cnt(int completed_Cnt) {
        this.completed_Cnt = completed_Cnt;
    }
    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }
}

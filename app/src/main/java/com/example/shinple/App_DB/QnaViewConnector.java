package com.example.shinple.App_DB;

import android.util.Log;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class QnaViewConnector extends Thread {
    String temp;
    String u_id;
    public QnaViewConnector(String u_id) {
        this.u_id = u_id;
    }

    public void run() {

        // http 요청을 쏴서 그에 대한 결과값을 받아옵니다.
        final String output = request("http://shinple.kr/app_db/qna_tbl_1.php?u_id="+u_id);

        // 결과값이 temp에 담깁니다.
        temp = output;

    }

    public String getTemp() {
        System.out.println("temp : "+ temp);
        return temp;
    }

    private String request(String urlStr) {
        StringBuilder output = new StringBuilder();
        try {
            URL url = new URL(urlStr);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            if (conn != null) {
                conn.setConnectTimeout(10000);
                conn.setRequestMethod("GET");
                conn.setDoInput(true);
                conn.setDoOutput(true);

                int resCode = conn.getResponseCode();
                if (resCode == HttpURLConnection.HTTP_OK) {
                    BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                    String line = null;
                    while (true) {
                        line = reader.readLine();
                        if (line == null) {
                            break;
                        }
                        output.append(line + "\n");
                    }

                    reader.close();
                    conn.disconnect();
                }
            }
        } catch (Exception ex) {
            Log.e("SampleHTTP", "Exception in processing response.", ex);
            ex.printStackTrace();
        }

        return output.toString();

    }
}


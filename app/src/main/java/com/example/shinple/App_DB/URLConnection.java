package com.example.shinple.App_DB;

import android.util.Log;

import com.example.shinple.App_DB.QnaURLConnector;
import com.example.shinple.App_DB.VideoUrlConnection;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class URLConnection extends Thread {
    String temp;
    QnaURLConnector url;

    public void run() {

        // http 요청을 쏴서 그에 대한 결과값을 받아옵니다.
        final String output = request("http://shinple.kr/app_db/account_tbl.php");

        // 결과값이 temp에 담깁니다.
        temp = output;
    }

    public String getTemp(){
        return temp;
    }

    private String request(String urlStr) {
        StringBuilder output = new StringBuilder();
        try {
            URL url = new URL(urlStr);
            HttpURLConnection conn = (HttpURLConnection)url.openConnection();
            if (conn != null) {
                conn.setConnectTimeout(10000);
                conn.setRequestMethod("GET");
                conn.setDoInput(true);
                conn.setDoOutput(true);

                int resCode = conn.getResponseCode();
                if (resCode == HttpURLConnection.HTTP_OK) {
                    BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream())) ;
                    String line = null;
                    while(true) {
                        line = reader.readLine();
                        if (line == null) {
                            break;
                        }
                        output.append(line + "\n");
                    }

                    reader.close();
                    conn.disconnect();
                }
            }
        } catch(Exception ex) {
            Log.e("SampleHTTP", "Exception in processing response.", ex);
            ex.printStackTrace();
        }

        return output.toString();
    }

}

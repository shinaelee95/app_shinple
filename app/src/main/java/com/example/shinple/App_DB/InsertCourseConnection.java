package com.example.shinple.App_DB;

import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class InsertCourseConnection extends Thread {

    String temp;
    String url;
    int c_id;
    int u_id;
    int c_period;

    public void connInsertCourse(int userId, int courseId, int period) {
        url = "http://shinple.kr/app_db/insert_course_tbl.php";
        c_id = courseId;
        u_id = userId;
        c_period = period;
    }

    public void run() {
        final String output = HttpPostData(url, c_id, u_id, c_period);
        temp = output;
    }

    public String getTemp(){
        return temp;
    }


    public String HttpPostData(String urlStr, int course_id, int user_id, int course_period) {
        String myResult="";
        System.out.println(urlStr);
        try {
            URL urlPost = new URL(urlStr);
            HttpURLConnection http = (HttpURLConnection) urlPost.openConnection();   // 접속
            //--------------------------
            //   전송 모드 설정 - 기본적인 설정이다
            //--------------------------
            http.setDefaultUseCaches(false);
            http.setDoInput(true);                         // 서버에서 읽기 모드 지정
            http.setDoOutput(true);                       // 서버로 쓰기 모드 지정
            http.setRequestMethod("POST");         // 전송 방식은 POST

            // 서버에게 웹에서 <Form>으로 값이 넘어온 것과 같은 방식으로 처리하라는 걸 알려준다
            http.setRequestProperty("content-type", "application/x-www-form-urlencoded");
            //--------------------------
            //   서버로 값 전송
            //--------------------------
            StringBuffer buffer = new StringBuffer();

            //사원 id 전달
            buffer.append("u_id").append("=").append(user_id).append("&");
            buffer.append("c_id").append("=").append(course_id).append("&");                 // php 변수에 값 대입
            buffer.append("c_period").append("=").append(course_period);

            System.out.println(buffer);
            OutputStreamWriter outStream = new OutputStreamWriter(http.getOutputStream(), "utf-8");
            PrintWriter writer = new PrintWriter(outStream);
            writer.write(buffer.toString());
            writer.flush();
            //--------------------------
            //   서버에서 전송받기
            //--------------------------
            InputStreamReader tmp = new InputStreamReader(http.getInputStream(), "utf-8");
            BufferedReader reader = new BufferedReader(tmp);
            StringBuilder builder = new StringBuilder();
            String str;
            while ((str = reader.readLine()) != null) {       // 서버에서 라인단위로 보내줄 것이므로 라인단위로 읽는다
                builder.append(str + "\n");                     // View에 표시하기 위해 라인 구분자 추가
            }
            myResult = builder.toString();                       // 전송결과를 전역 변수에 저장
            System.out.println("Send Result : " + myResult);
        } catch (MalformedURLException e) {
            Log.e("Error Tag : ",  e.toString());
        } catch (IOException e) {
            Log.e("Error Tag : ",   e.toString());
        } // try
        return  myResult;
    }
}

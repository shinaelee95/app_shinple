//package com.example.shinple.App_DB;
//
//import android.util.Log;
//
//import java.io.BufferedReader;
//import java.io.IOException;
//import java.io.InputStreamReader;
//import java.io.OutputStreamWriter;
//import java.io.PrintWriter;
//import java.net.HttpURLConnection;
//import java.net.MalformedURLException;
//import java.net.URL;
//
//public class FullCourseConnection extends  Thread {
//    String temp;
//    String url;
//    int main_id;
//    int sub_id;
//    int user_id;
//    int course_id;
//    int is_completed;
//
//
//    public void jobOne(int main_id) {
//        url = "http://shinple.kr/app_db/course_tbl.php";
//        main_id= main_id;
//    }
//
//    public void jobTwo(int main_id) {
//        url = "http://shinple.kr/app_db/course_tbl.php";
//        main_id = main_id;
//    }
//    public void jobThree(int main_id) {
//        url = "http://shinple.kr/app_db/course_tbl.php";
//        main_id = main_id;
//        is_completed = 0;
//    }
//
//    public void jobFour(int main_id) {
//        url = "http://shinple.kr/app_db/course_tbl.php";
//        main_id = main_id;
//        is_completed = 1;
//    }
//    public void capOne(int main_id) {
//        url = "http://shinple.kr/app_db/course_tbl.php";
//        main_id = main_id;
//    }
//
//    public void capTwo(int main_id) {
//        url = "http://shinple.kr/app_db/course_tbl.php";
//        main_id = main_id;
//    }
//    public void capThree(int main_id) {
//        url = "http://shinple.kr/app_db/course_tbl.php";
//        main_id = main_id;
//        is_completed = 0;
//    }
//
//    public void capFour(int main_id) {
//        url = "http://shinple.kr/app_db/course_tbl.php";
//        main_id = main_id;
//        is_completed = 1;
//    }
//    public void languageOne(int main_id) {
//        url = "http://shinple.kr/app_db/course_tbl.php";
//        main_id = main_id;
//    }
//
//    public void languageTwo(int main_id) {
//        url = "http://shinple.kr/app_db/course_tbl.php";
//        main_id = main_id;
//    }
//    public void languageThree(int main_id) {
//        url = "http://shinple.kr/app_db/course_tbl.php";
//        user_id = u_id;
//        is_completed = 0;
//    }
//
//    public void languageFour(int main_id) {
//        url = "http://shinple.kr/app_db/course_tbl.php";
//        user_id = u_id;
//        is_completed = 1;
//    }
//    public void certiOne(int main_id) {
//        url = "http://shinple.kr/app_db/course_tbl.php";
//        user_id = u_id;
//    }
//
//    public void certiTwo(int main_id) {
//        url = "http://shinple.kr/app_db/course_tbl.php";
//        user_id = u_id;
//    }
//    public void certiThree(int main_id) {
//        url = "http://shinple.kr/app_db/course_tbl.php";
//        user_id = u_id;
//        is_completed = 0;
//    }
//
//    public void certiFour(int main_id) {
//        url = "http://shinple.kr/app_db/course_tbl.php";
//        user_id = u_id;
//        is_completed = 1;
//    }
//
//
//
//    public void run() {
//       final String output = HttpPostData(url, user_id);
//        temp = output;
//    }
//
//    public String getTemp(){
//        return temp;
//    }
//
//
//
//    public String HttpPostData(String urlStr, int u_id) {
//        String myResult="";
//        try {
//            URL urlPost = new URL(urlStr);
//            HttpURLConnection http = (HttpURLConnection) urlPost.openConnection();   // 접속
//            //--------------------------
//            //   전송 모드 설정 - 기본적인 설정이다
//            //--------------------------
//            http.setDefaultUseCaches(false);
//            http.setDoInput(true);                         // 서버에서 읽기 모드 지정
//            http.setDoOutput(true);                       // 서버로 쓰기 모드 지정
//            http.setRequestMethod("POST");         // 전송 방식은 POST
//
//            // 서버에게 웹에서 <Form>으로 값이 넘어온 것과 같은 방식으로 처리하라는 걸 알려준다
//            http.setRequestProperty("content-type", "application/x-www-form-urlencoded");
//            //--------------------------
//            //   서버로 값 전송
//            //--------------------------
//            StringBuffer buffer = new StringBuffer();
//
//            //강좌의 main_id 전송
//            buffer.append("main_id").append("=").append(u_id);                 // php 변수에 값 대입
//
//            System.out.println(buffer);
//            OutputStreamWriter outStream = new OutputStreamWriter(http.getOutputStream(), "utf-8");
//            PrintWriter writer = new PrintWriter(outStream);
//            writer.write(buffer.toString());
//            writer.flush();
//            //--------------------------
//            //   서버에서 전송받기
//            //--------------------------
//            InputStreamReader tmp = new InputStreamReader(http.getInputStream(), "utf-8");
//            BufferedReader reader = new BufferedReader(tmp);
//            StringBuilder builder = new StringBuilder();
//            String str;
//            while ((str = reader.readLine()) != null) {       // 서버에서 라인단위로 보내줄 것이므로 라인단위로 읽는다
//                builder.append(str + "\n");                     // View에 표시하기 위해 라인 구분자 추가
//            }
//            myResult = builder.toString();                       // 전송결과를 전역 변수에 저장
//            //System.out.println("Send Result : " + myResult);
//        } catch (MalformedURLException e) {
//            Log.e("Error Tag : ",  e.toString());
//        } catch (IOException e) {
//            Log.e("Error Tag : ",   e.toString());
//        } // try
//        return  myResult;
//    }
//}

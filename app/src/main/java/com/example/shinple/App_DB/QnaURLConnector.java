package com.example.shinple.App_DB;

import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class QnaURLConnector extends Thread {
    //String temp;
    String temp;
    String url;
    int q_type;
    String  q_title;
    String q_contents;
    int user_id;

    public void setData(int type, String title, String contents, int u_id) {
        url = "http://shinple.kr/app_db/qna_insert.php";
        q_type=type;
        q_title = title;
        q_contents= contents;
        user_id = u_id;
    }

    public void run() {
        final String output = HttpPostData(url, q_type, q_title, q_contents, user_id);
        temp = output;
    }

    public String getTemp(){
        return temp;
    }

    public String HttpPostData(String urlStr, int type, String title, String contents, int u_id) {
        String myResult="";
        System.out.println(("POST 시작"));
        try {

            URL urlPost = new URL(urlStr);
            System.out.println("urlPost : " + urlPost);

            HttpURLConnection http = (HttpURLConnection) urlPost.openConnection();   // 접속
            //--------------------------
            //   전송 모드 설정 - 기본적인 설정이다
            //--------------------------
            http.setDefaultUseCaches(false);
            http.setDoInput(true);                         // 서버에서 읽기 모드 지정
            http.setDoOutput(true);                       // 서버로 쓰기 모드 지정
            http.setRequestMethod("POST");         // 전송 방식은 POST

            // 서버에게 웹에서 <Form>으로 값이 넘어온 것과 같은 방식으로 처리하라는 걸 알려준다
            http.setRequestProperty("content-type", "application/x-www-form-urlencoded");
            //--------------------------
            //   서버로 값 전송
            //--------------------------
            StringBuffer buffer = new StringBuffer();
            //강좌 id 전달
            buffer.append("type").append("=").append(type).append("&");
            buffer.append("title").append("=").append(title).append("&");
            buffer.append("contents").append("=").append(contents).append("&");
            buffer.append("u_id").append("=").append(u_id);                 // php 변수에 값 대입

            OutputStreamWriter outStream = new OutputStreamWriter(http.getOutputStream(), "utf-8");
            PrintWriter writer = new PrintWriter(outStream);
            writer.write(buffer.toString());
            writer.flush();
            //--------------------------
            //   서버에서 전송받기
            //--------------------------
            InputStreamReader tmp = new InputStreamReader(http.getInputStream(), "utf-8");
            BufferedReader reader = new BufferedReader(tmp);
            StringBuilder builder = new StringBuilder();
            String str;
            while ((str = reader.readLine()) != null) {       // 서버에서 라인단위로 보내줄 것이므로 라인단위로 읽는다
                builder.append(str + "\n");                     // View에 표시하기 위해 라인 구분자 추가
            }
            myResult = builder.toString();                       // 전송결과를 전역 변수에 저장
            //System.out.println("Send Result : " + myResult);
        } catch (MalformedURLException e) {
            Log.e("Error Tag : ",  e.toString());
        } catch (IOException e) {
            Log.e("Error Tag : ",   e.toString());
        } // try
        return  myResult;
    }
}

package com.example.shinple.Functions;

import android.provider.CalendarContract;

import com.example.shinple.Adapter.LearningStatusAdapter;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class TakesCourseFunc {

    private static  ArrayList<Map<String, String>> learningList;
    private  static Map<String, String > mapLearing;

    // JSON 데이터를 파싱합니다.
    // URLConnector로부터 받은 String이 JSON 문자열이기 때문입니다.
    public static ArrayList<Map<String, String>> parseCourseJSON(String target){
        try {
            learningList = new ArrayList<>();
            JSONObject json = new JSONObject(target);
            JSONArray arr = json.getJSONArray("result");

            if(arr != null)
            {
                for(int i = 0; i < arr.length(); i++){
                    mapLearing =  new HashMap<>();
                    JSONObject obj = arr.getJSONObject(i);
                    mapLearing.put("c_id", obj.getString("c_id"));
                    mapLearing.put("sub_cate_name", obj.getString("sub_cate_name"));
                    mapLearing.put("c_name", obj.getString("c_name"));
                    mapLearing.put("start_date", obj.getString("start_date"));
                    mapLearing.put("end_date", obj.getString("end_date"));
                    mapLearing.put("completedCnt", obj.getString("completedCnt"));
                    mapLearing.put("totalCnt", obj.getString("totalCnt"));
                    mapLearing.put("thumbnail", obj.getString("thumbnail"));
                    mapLearing.put("status", obj.getString("status"));

                    learningList.add(mapLearing);
                    System.out.println(mapLearing);
                }
            }
            return learningList;
        }
        catch(Exception e){
            System.out.println("json exception");

            e.printStackTrace();
        }
        return null;
    }

    public static  LearningStatusAdapter setLearingData(LearningStatusAdapter adapter, ArrayList<Map<String, String>> learningList){
        for (Map<String, String> map : learningList) {
            adapter.addItem( Integer.parseInt(map.get("c_id")), map.get("sub_cate_name"), map.get("thumbnail") , map.get("c_name")
                    , map.get("start_date"), map.get("end_date"),map.get("completedCnt"), map.get("totalCnt"), map.get("status") ) ;
        }
        return  adapter;
    }


}

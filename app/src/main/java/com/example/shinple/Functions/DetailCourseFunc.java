package com.example.shinple.Functions;

import com.example.shinple.Adapter.CourseDetailListViewAdapter;
import com.example.shinple.ListVO.DetailCourseVO;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class DetailCourseFunc {

    private static ArrayList<Map<String, String>> courseVideoList;
    private  static Map<String, String > mapDetailCourse;

    // JSON 데이터를 파싱합니다.
    // URLConnector로부터 받은 String이 JSON 문자열이기 때문입니다.
    public static DetailCourseVO parseDetailCourseJSON(String target){
        try {
            DetailCourseVO detailData = new DetailCourseVO();
            JSONObject json = new JSONObject(target);
            JSONArray arr = json.getJSONArray("result");

            if(arr != null)
            {
                for(int i = 0; i < arr.length(); i++){
                    mapDetailCourse =  new HashMap<>();
                    JSONObject obj = arr.getJSONObject(i);
                    detailData.setCourseId(Integer.parseInt( obj.getString("c_id")));
                    //detailData.setSubCategory(obj.getString("sub_cate_name"));
                    detailData.setTitle(obj.getString("c_name"));
                    detailData.setThumbnail(obj.getString("thumbnail"));
                    detailData.setCourseInfo(obj.getString("c_info"));
                    detailData.setInstructor(obj.getString("instructor"));
                    detailData.setTakesCnt(Integer.parseInt(obj.getString("view_count")));
                    detailData.setPeriod(Integer.parseInt(obj.getString("c_period")));
                }
            }
            return detailData;
        }
        catch(Exception e){
            System.out.println("json exception");

            e.printStackTrace();
        }
        return null;
    }

    public static ArrayList<Map<String, String>> parseDetailCourseVideoJSON(String target){

        System.out.println("비디오리스트 JSON 시작");
        try {
            courseVideoList = new ArrayList<>();
            JSONObject json = new JSONObject(target);
            JSONArray arr = json.getJSONArray("resultList");

            if(arr != null)
            {
                for(int i = 0; i < arr.length(); i++){
                    mapDetailCourse =  new HashMap<>();
                    JSONObject obj = arr.getJSONObject(i);
                    mapDetailCourse.put("v_id", obj.getString("v_id"));
                    mapDetailCourse.put("v_title", obj.getString("v_title"));
                    mapDetailCourse.put("total_time", obj.getString("total_time"));
                    courseVideoList.add(mapDetailCourse);
                }
            }
            System.out.println(courseVideoList);
            return courseVideoList;
        }
        catch(Exception e){
            System.out.println("json exception");

            e.printStackTrace();
        }
        return null;
    }

/*    public static DetailCourseAdapter  setCourseData(Map<String, String> map ){
            adapter.addCourseItem( map.get("c_id"), map.get("sub_cate_name"), map.get("c_name") , map.get("thumbnail"), map.get("c_info"),map.get("instructor"), map.get("view_count"),map.get("c_period")) ;
            return adapter;
    }*/

    public static CourseDetailListViewAdapter setVideoListData(CourseDetailListViewAdapter adapter, ArrayList<Map<String, String>> courseVideoList){
        for (Map<String, String> map : courseVideoList) {
            adapter.addVideoItem( map.get("v_id"), map.get("v_title"),map.get("total_time")) ;
        }
        return  adapter;
    }
}

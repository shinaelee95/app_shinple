package com.example.shinple.Functions;

        import android.widget.TextView;

        import org.json.JSONArray;
        import org.json.JSONObject;

public class Notice_SslFunc {
    // JSON 데이터를 파싱합니다.(가져온 DB 정보 중 필요한 부분 파씽)
    // URLConnector로부터 받은 String이 JSON 문자열이기 때문입니다.
    public static String ParseJSON(String target, TextView[] textRes, String[] column,int pass_n_id){
        try {
            JSONObject json = new JSONObject(target);
            JSONArray arr = json.getJSONArray("result");

            System.out.println("n_id 값 = "+pass_n_id);
            for(int i = 0; i < arr.length(); i++){
                JSONObject json2 = arr.getJSONObject(i);
                if((Integer.parseInt(json2.getString("n_id"))) == pass_n_id){
                    textRes[0].setText(json2.getString(column[0]));
                    textRes[1].setText(json2.getString(column[1]));
                    textRes[2].setText(json2.getString(column[2]));
                    textRes[3].setText(json2.getString(column[3]));
                }
            }
            return "";
        }
        catch(Exception e){
            e.printStackTrace();
        }
        return null;
    }
}
